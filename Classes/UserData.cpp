#include "UserData.h"
#include "SimpleAudioEngine.h"
#include <string>
#include <fstream>

#define MODE_SAVE_SIZE 300
#define URL_DATASAVE "game_data.sav"
#define IRASEMODE false
USING_NS_CC;
UserData* UserData::m_instance = NULL;

UserData::UserData()
{
	m_isPlaying = false;
	m_levelFileToLoad = "";
	loadDataFromFile();
}
UserData* UserData::getInstance()
{
	if (m_instance == NULL)
	{
		m_instance = new UserData();
	}
	return m_instance;
}
UserData::~UserData()
{

}
void UserData::saveLevelIntoFile()
{
	std::string path = choosePathPackage(GameManager::getInstance()->getCurrentMode());
	char* filePath = new char[MODE_SAVE_SIZE];
	strcpy(filePath, path.c_str());
	strcat(filePath, getNameLevelFileToSave().c_str());

	std::ofstream fs(filePath, std::ios::binary | std::ios::out);

	CC_SAFE_DELETE_ARRAY(filePath);

	// copy info 
	this->setSaveFile(GameManager::getInstance()->getLevelState());

	// save 
	fs.write((char*)&m_levelSave, sizeof(LevelState));
	fs.close();
}
void UserData::loadLevelFromFile()
{
	auto filename = getNameLevelFileToLoad();
	std::string folderpath = choosePathPackage(GameManager::getInstance()->getCurrentMode());
	std::string path = folderpath  + filename;

	std::ifstream infile(path, std::ios::in | std::ios::binary);
	LevelState m_levelSave;
	if (!infile)
	{
		memset(&m_levelSave.width, 0, sizeof(int));
		memset(&m_levelSave.height, 0, sizeof(int));
		memset(&m_levelSave.scaleBoard, 0, sizeof(int));
		memset(&m_levelSave.scalePipe, 0, sizeof(int));
		memset(&m_levelSave.board, 0, sizeof(int)*(20*20));
		memset(&m_levelSave.hole_Color, 0, sizeof(int)*(20*20));
		memset(&m_levelSave.maxMove, 0, sizeof(int));
		memset(&m_levelSave.bestMove, 0, sizeof(int));
		memset(&m_levelSave.starEarned, 0, sizeof(int));
	}
	else
	{
		infile.read((char*)&m_levelSave, sizeof(LevelState));
		GameManager::getInstance()->setLevelState(m_levelSave);
		infile.close();
	}
}
//void UserData::saveSateIntoFile()
//{
//	std::string path = cocos2d::FileUtils::getInstance()->getWritablePath();
//	char* filePath = new char[MODE_SAVE_SIZE];
//	strcpy(filePath, path.c_str());
//	strcat(filePath, getSaveURLClassic().c_str());
//	std::ofstream fs(filePath, std::ios::binary | std::ios::out);
//	CC_SAFE_DELETE_ARRAY(filePath);
//	// copy info 
//	this->setSaveFile(GameManager::getInstance()->getStateSave());
//	// save 
//	fs.write((char*)&m_stateSave, sizeof(StateSave));
//	fs.write((char*)&m_isPlaying, sizeof(bool));
//	fs.close();
//	
//}

//void UserData::loadStateFromFile()
//{
//	std::string path = cocos2d::FileUtils::getInstance()->getWritablePath();
//	char* filePath = new char[MODE_SAVE_SIZE];
//	strcpy(filePath, path.c_str());
//	strcat(filePath, getSaveURLClassic().c_str());
//	std::ifstream infile(filePath, std::ios::in | std::ios::binary);
//	CC_SAFE_DELETE_ARRAY(filePath);
//
//	StateSave msave;
//	if (!infile || IRASEMODE)
//	{
//		memset(&msave.stateRing, 0, sizeof(msave.stateRing));
//		memset(&msave.stateChild, 0, sizeof(msave.stateChild));
//		memset(&msave.ringDrag_state, 0, sizeof(msave.ringDrag_state));
//		memset(&msave.ringDrag_stateChild, 0, sizeof(msave.ringDrag_stateChild));
//		memset(&msave.ringDrag_stateType, 0, sizeof(msave.ringDrag_stateType));
//		memset(&msave.isPlaying, false, sizeof(bool));
//		setSaveFile(msave);
//	}
//	else
//	{
//		infile.read((char*)&msave, sizeof(StateSave));
//		infile.read((char*)&m_isPlayingClassic, sizeof(bool));
//		GameManager::getInstance()->setStateSave(msave);
//		infile.close();
//		setSaveFile(msave);
//	}
//}
void UserData::saveDataIntoFile()
{
	std::string path = cocos2d::FileUtils::getInstance()->getWritablePath();
	char* filePath = new char[MODE_SAVE_SIZE];
	strcpy(filePath, path.c_str());
	strcat(filePath, getURLSaveData().c_str());
	std::ofstream fs(filePath, std::ios::binary | std::ios::out);
	CC_SAFE_DELETE_ARRAY(filePath);
	
	this->setDataFile(GameManager::getInstance()->getDataSave());
	// save 
	fs.write((char*)&m_dataSave, sizeof(DataSave));
	fs.close();
}


void UserData::loadDataFromFile()
{
	std::string path = cocos2d::FileUtils::getInstance()->getWritablePath();
	char* filePath = new char[MODE_SAVE_SIZE];
	strcpy(filePath, path.c_str());
	strcat(filePath, getURLSaveData().c_str());
	std::ifstream infile(filePath, std::ios::in | std::ios::binary);
	CC_SAFE_DELETE_ARRAY(filePath);
	DataSave m_data;
	if (!infile)
	{
		memset(&m_data.currentLevel, 1, sizeof(int));
		memset(&m_data.passedLevel, 0, sizeof(int));
		memset(&m_data.currentMode, 1, sizeof(int));
		memset(&m_data.passedMode, 0, sizeof(int));
		memset(&m_data.allStarEarned, 0, sizeof(int));
	}
	else
	{
		infile.read((char*)&m_data, sizeof(DataSave));
		GameManager::getInstance()->setDataSave(m_data);
		infile.close();
	}
}

void UserData::reset()
{
	/*StateSave msave;
	memset(&msave.stateRing, 0, sizeof(msave.stateRing));
	memset(&msave.stateChild, 0, sizeof(msave.stateChild));
	memset(&msave.isPlaying, false, sizeof(msave.isPlaying));
	GameManager::getInstance()->setStateSave(msave);
	this->setSaveFile(msave);
	this->saveSateIntoFile();*/
}

bool UserData::getIsPlaying()
{
	return m_isPlaying;
}
void UserData::setIsPlaying(bool check)
{
	m_isPlaying = check;
}

std::string UserData::getURLSaveData()
{
	return URL_DATASAVE;
}

void UserData::setDataFile(DataSave dataSave)
{
	m_dataSave = dataSave;
}


DataSave UserData::getDataFile()
{
	return m_dataSave;
}

std::string UserData::getNameLevelFileToSave()
{
	return m_levelFileToSave;
}
void UserData::setNameLevelFileToSave(std::string name)
{
	m_levelFileToSave = name;
}
std::string UserData::getNameLevelFileToLoad()
{
	return m_levelFileToLoad;
}
void UserData::setNameLevelFileToLoad(std::string name)
{
	m_levelFileToLoad = name;
}

void UserData::setSaveFile(LevelState newStateSave)
{
	m_levelSave = newStateSave;
}

LevelState UserData::getSaveFile()
{
	return m_levelSave;
}

std::string UserData::choosePathPackage(int mode)
{
	switch (mode)
	{
	case 1:
		return PATH_NOVICE_LEVELS;
		break;
	case 2:
		return PATH_BEGINER_LEVELS;
		break;
	case 3:
		return PATH_ADVANCE_LEVELS;
		break;
	case 4:
		return PATH_EXPERT_LEVELS;
		break;
	case 5:
		return PATH_MASTER_LEVELS;
		break;
	}
	
}
