#include "HomeScene.h"
#include "SimpleAudioEngine.h"
#include "Definition.h"
#include "ScenePlay.h"
#include "ModeScene.h"
USING_NS_CC;

Scene* HomeScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = HomeScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool HomeScene::init()
{
	if (!LayerColor::initWithColor(Color4B(109, 119, 247, 255)))
	{
		return false;
	}
	
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	//Button Play
	auto buttonPlay = MenuItemImage::create(BTN_HOME_PLAY, BTN_HOME_PLAY, CC_CALLBACK_0(HomeScene::goToLevelsScene, this));
	buttonPlay->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y + 20);
	buttonPlay->setScale(2);

	m_menuButton = Menu::create(buttonPlay, NULL);
	m_menuButton->setPosition(Vec2::ZERO);
	this->addChild(m_menuButton);
	this->scheduleUpdate();
	return true;
}
void HomeScene::goToLevelsScene()
{
	UserData::getInstance()->loadDataFromFile();
	auto scene = ModeScene::createScene();
	Director::getInstance()->replaceScene(TransitionProgressInOut::create(0.5f, scene));
}
