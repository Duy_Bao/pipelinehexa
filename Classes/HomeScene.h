#ifndef __HOME_SCENE_H__
#define __HOME_SCENE_H__

#include "cocos2d.h"
#include "ScenePlay.h"
#include "Definition.h"
using namespace cocos2d;
class HomeScene : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void goToLevelsScene();
	void goToEditorScene();
	CREATE_FUNC(HomeScene);
private:
	Menu *m_menuButton;
	Sprite *m_background;
};

#endif // __MENU_SCENE_H__
