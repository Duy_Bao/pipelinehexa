#ifndef __BOARD_H__
#define __BOARD_H__

#include "cocos2d.h"
#include "Pipe.h"
#include "Definition.h"
#include "GameManager.h"
class Board
{
public:
	Board(Layer *layer, bool check);
	~Board();
	void initBoard();
	void initBoardFromFile();
	void loadHoleIntoBoard();
	void intiHolesMatrix();
	void initPipesMatrix();
	cocos2d::Color3B chooseColor(COLOR inColor);
	std::vector<Vec2> chooseListBaseColor(COLOR inColor);
	void clearAllListColor();
	void copyListBaseColor(COLOR inColor, std::vector<Vec2> listVec2);
	bool isNeighbour(int baseCol, int baseRow, int nextCol, int nextRow);
	int checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList);
	void deleteAllPipeSameColor(COLOR color);
	void drawPipeOnList(std::vector<Vec2> listVec2);
	void choosePipeAndCreateOnHole(Vec2 prePos, Vec2 nextPos);
	void choosePipeAndCreate(Vec2 prePos, Vec2 nextPos, std::vector<cocos2d::Vec2> listVec2);
	void choosePipeAndCreateAtCurvePoint();
	int getRotationForGoBackWard(DIR preDirection);
	void showHint();
	void markBoardByColor(std::vector<Vec2> listVec2);
	cocos2d::Color3B chooseColorToMark(COLOR incolor);
	bool checkFillBoard();
	float calculatePercentFillBoard();
	int randomRange(int min, int max);
	Sprite*** getEmptyMatrix();
	Pipe*** getHolesMatrix();
	Pipe*** getPipesMatrix();
	Point getPositionHexa(int i, int j);
	int getPreTouchCol();
	void setPreTouchCol(int newValue);
	int getPreTouchRow();
	void setPreTouchRow(int newValue);
	int getNextTouchCol();
	void setNextTouchCol(int newValue);
	int getNextTouchRow();
	void setNextTouchRow(int newValue);
	bool getMeetSameHole();
	void setMeetSameHole(bool newValue);
	void resetTouchPos();
	std::vector<Vec2> getListRedPipe();
	std::vector<Vec2> getListBluePipe();
	std::vector<Vec2> getListGreenPipe();
	std::vector<Vec2> getListYellowPipe();
	std::vector<Vec2> getListPurplePipe();
private:
	int preTouch_col;
	int preTouch_row;
	int nextTouch_col;
	int nextTouch_row;
	int widthBoard;
	int heightBoard;
	float scaleBoard;
	Size sizeBoard;
	bool meetSameHole;
	std::vector<Vec2> listRedPipe;
	std::vector<Vec2> listBluePipe;
	std::vector<Vec2> listGreenPipe;
	std::vector<Vec2> listYellowPipe;
	std::vector<Vec2> listPurplePipe;
	Layer *m_layer;
	Sprite ***m_emptyMatrix;
	Pipe ***m_holesMatrix;
	Pipe ***m_pipesMatrix;
};

#endif // __HELLOWORLD_SCENE_H__
	