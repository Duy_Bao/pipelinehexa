#ifndef __LEVEL_SUCCESS_LAYER_H__
#define __LEVEL_SUCCESS_LAYER_H__

#include "cocos2d.h"
#include <ctime>
using namespace cocos2d;
class LevelSuccessLayer : public cocos2d::Layer
{
public:
    virtual bool init();
	void show();
	void hide();
	void actionAppear(Vec2 posEnd);
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void showStarAndButton(int numOfStar);
	CREATE_FUNC(LevelSuccessLayer);
private:
	void restartGame(cocos2d::Ref *sender);
	void nextLevel(cocos2d::Ref *sender);
	void swallowTouch();
private:
	Menu *levelSuccessMenu;
	MenuItemImage *btn_LevelSuccess_Restart;
	MenuItemImage *btn_LevelSuccess_Menu;
	MenuItemImage* btn_LevelSuccess_Next;
	Label *labelComplete;
	UserDefault *def;
	EventListenerTouchOneByOne* m_pTouchListener;
	int move;
	int bestMove;
	bool isShow;
	Sprite *firstStar;
	Sprite *secondStar;
	Sprite *thirdStar;
};

#endif // __GAME_OVER_LAYER_H__
