#ifndef __GAME_MANAGER_H__
#define __GAME_MANAGER_H__

#include "cocos2d.h"

#include <iostream>
#include "Definition.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace experimental;
class GameManager
{
	private:
		static GameManager* m_instance;
		GameManager();
	public:
		static GameManager* getInstance();
		~GameManager();
		COLOR getColorCurrent();
		void setColorCurrent(COLOR newValue);

		bool getBeginFromHole();
		void setBeginFromHole(bool newValue);

		bool getEndDrag();
		void setEndDrag(bool newValue);

		bool getCheckedHole();
		void setCheckedHole(bool newValue);

		DIR getNextDirection();
		void setNextDirection(DIR newValue);

		DIR getLastDirection();
		void setLastDirection(DIR newValue);

		TYPEPIPE getTypePipeCurve();
		void setTypePipeCurve(TYPEPIPE newValue);

		int getPipeCurveRotation();
		void setPipeCurveRotation(int newValue);

		int getNextPipeCurveRotation();
		void setNextPipeCurveRotation(int newValue);

		LevelState getLevelState();
		void setLevelState(LevelState newLevel);

		DataSave getDataSave();
		void setDataSave(DataSave newLevel);

		int getCurrentPackage();
		void setCurrentPackage(int newValue);

		int getCurrentLevel();
		void setCurrentLevel(int newValue);

		int getCurrentMode();
		void setCurrentMode(int newValue);

		int getStarEarn();
		void setStarEarn(int newValue);
		int getNumOfMove();
		void setNumOfMove(int newValue);

		bool getLoadLevel();
		void setLoadLevel(bool newValue);

		int getWidth();
		void setWidth(int newValue);

		int getHeight();
		void setHeight(int newValue);

		float getScaleBoard();
		void setScaleBoard(float newValue);

		float getScalePipe();
		void setScalePipe(float newValue);
	private:
		DataSave m_dataSave;
		LevelState m_levelState;
		COLOR colorCurrent;
		int currentLevel;
		int currentMode;
		int currentPackage;
		bool beginFromHole;
		bool endDrag;
		bool isLoadLevel;
		bool checkedHole;
		int widthBoard;
		int heightBoard;
		float scaleBoard;
		float scalePipe;
		DIR nextDirection;
		DIR lastDirection;
		TYPEPIPE typePipeCurve;
		int pipeCurveRotation;
		int nextpipeCurveRotation;
		int starEarnedInLevel;
		int numOfMove;
};

#endif //  _SINGLETON_H_
