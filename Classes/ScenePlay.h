#ifndef __SCENE_PLAY_H__
#define __SCENE_PLAY_H__


#include "cocos2d.h"
#include <iostream>
#include <ctime>
#include "Board.h"
#include "Definition.h"
#include "GameManager.h"
#include "LevelSuccessLayer.h"
#include "UserData.h"
class ScenePlay : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void initBoard();
	void initDefine();
	void initButton();
	void initStar();
	void initLayer();
	void initLabel();
	void update(float dt);
	void touchEvent();
	void buttonBack(Ref *node);
	void buttonPreLevel(Ref *node);
	void buttonNextLevel(Ref *node);
	void buttonHint(Ref *node);
	void buttonRestart(Ref *node);
	int checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList);
	void handleTouch(int x, int y);
	void updateMoveAndFillAfterTouchEnded();
	void popVector(std::vector<Vec2> &list, int id);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void checkPassLevelAndSaveData();
	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchCancelled(Touch *touch, cocos2d::Event *event);
	int calculateStarEarn();
	CREATE_FUNC(ScenePlay);
private:
	Board *board;
	Menu *menu;
	MenuItemImage *btn_back;
	MenuItemImage *btn_nextLevel;
	MenuItemImage *btn_preLevel;
	MenuItemImage *btn_restart;
	MenuItemImage *btn_hint;
	Label *fillPercentLabel;
	Label *moveLabel;
	Label *bestMoveLabel;
	Label *currentLevelLabel;
	Sprite *firstStar;
	Sprite *secondStar;
	Sprite *thirdStar;
	LevelSuccessLayer *successLayer;
	UserDefault *def;
	std::vector<cocos2d::Vec2> listVec2;
	int widthBoard;
	int heightBoard;
	float scaleBoard;
	float scalePipe;
	bool gameOver;
	bool showRedHint;
	bool showYellowHint;
	bool showGreenHint;
	bool showBlueHint;
	bool showPurpleHint;
	int numOfMaxMove;
	int numOfMove;
	int bestNumOfMove;
	int numOfStarEarned;
	
};

#endif // __SCENE_PLAY_H__
