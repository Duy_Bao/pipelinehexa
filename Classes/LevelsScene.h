#ifndef __LEVELS_SCENE_H__
#define __LEVELS_SCENE_H__

#include <iostream>
#include <ctime>
#include "cocos2d.h"
class LevelsScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
	void backToModeScene();
	void goToScenePlay(int level);
	CREATE_FUNC(LevelsScene);
private:
	void onBtnLevelClicked(Ref* ndo);
	MenuItemImage *btn_back;
	Menu *menu;
};

#endif // __LEVELS_SCENE_H__
