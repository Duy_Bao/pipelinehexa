#include "Board.h"
#include "SimpleAudioEngine.h"
#include "Definition.h"
#include "GameManager.h"
USING_NS_CC;


Board::Board(Layer *layer, bool check)
{
	m_layer = layer;
	preTouch_col = 0;
	preTouch_row = 0;
	nextTouch_col = 0;
	nextTouch_row = 0;
	meetSameHole = false;
	widthBoard = GameManager::getInstance()->getWidth();
	heightBoard = GameManager::getInstance()->getHeight();
	scaleBoard = GameManager::getInstance()->getScaleBoard();
	sizeBoard = Size(Sprite::create(BOARD)->getContentSize()*scaleBoard);
	initBoardFromFile();
	loadHoleIntoBoard();
	initPipesMatrix();
	m_layer->setContentSize(WIN_SIZE);
}

Board::~Board()
{

}

void Board::initBoard()
{
	m_emptyMatrix = new Sprite **[widthBoard];
	for (int i = 0; i < widthBoard; i++)
	{
		m_emptyMatrix[i] = new  Sprite *[heightBoard];
		for (int j = 0; j < heightBoard; j++)
		{
			auto emptyCell = Sprite::create(BOARD);
			emptyCell->setScale(scaleBoard);
			emptyCell->setColor(Color3B::WHITE);
			emptyCell->setPosition(getPositionHexa(i, j).x*sizeBoard.width, getPositionHexa(i, j).y*sizeBoard.height - sizeBoard.height / 2);
			m_emptyMatrix[i][j] = emptyCell;
			m_emptyMatrix[i][j]->setOpacity(100);
			m_layer->addChild(m_emptyMatrix[i][j], BOARDMATRIX);
		}
	}
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			auto lbel = Label::createWithTTF(StringUtils::format("%i , %i", i, j), "fonts/arial.ttf", 50);
			lbel->setColor(Color3B::RED);
			lbel->setPosition(m_emptyMatrix[i][j]->getPosition()/5
			);
			m_emptyMatrix[i][j]->addChild(lbel);
		}
	}
}
void Board::initBoardFromFile()
{
	auto temp = GameManager::getInstance()->getLevelState();
	m_emptyMatrix = new Sprite **[widthBoard];
	for (int i = 0; i < widthBoard; i++)
	{
		m_emptyMatrix[i] = new  Sprite *[heightBoard];
		for (int j = 0; j < heightBoard; j++)
		{
			if (temp.board[i][j] >= 0)
			{
				if (temp.board[i][j] == 1)
				{
					auto emptyCell = Sprite::create(BOARD);
					emptyCell->setScale(scaleBoard);
					emptyCell->setColor(Color3B::WHITE);
					emptyCell->setPosition(getPositionHexa(i, j).x*sizeBoard.width, getPositionHexa(i, j).y*sizeBoard.height - sizeBoard.height / 2);
					m_emptyMatrix[i][j] = emptyCell;
					m_emptyMatrix[i][j]->setOpacity(100);
					m_layer->addChild(m_emptyMatrix[i][j], BOARDMATRIX);
				}
				else
				{
					m_emptyMatrix[i][j] = nullptr;
				}
			}
		}
	}
}
void Board::loadHoleIntoBoard()
{
	m_holesMatrix = new Pipe **[widthBoard];
	auto temp = GameManager::getInstance()->getLevelState();
	for (int i = 0; i < widthBoard; i++)
	{
		m_holesMatrix[i] = new  Pipe *[heightBoard];
		for (int j = 0; j < heightBoard; j++)
		{
			if (temp.board[i][j] >= 0)
			{
				if (temp.board[i][j] == 1)
				{
					if (temp.hole_Color[i][j] == 0)
					{
						m_holesMatrix[i][j] = nullptr;
					}
					else
					{
						auto hole = new Pipe(HOLE, (COLOR)temp.hole_Color[i][j], i, j);
						hole->setPosition(this->getEmptyMatrix()[i][j]->getPosition());
						m_holesMatrix[i][j] = hole;
						m_layer->addChild(m_holesMatrix[i][j], HOLEMATRIX);
					}
				}
			}
		}
	}
}
void Board::initPipesMatrix()
{
	m_pipesMatrix = new Pipe **[widthBoard];
	for (int i = 0; i < widthBoard; i++)
	{
		m_pipesMatrix[i] = new  Pipe *[heightBoard];
		for (int j = 0; j < heightBoard; j++)
		{
			m_pipesMatrix[i][j] = nullptr;
		}
	}
}

cocos2d::Color3B Board::chooseColor(COLOR inColor)
{
	Color3B color;
	switch (inColor)
	{
	case COLOR::RED:
	{
		color = Color3B::RED;
		break;
	}
	case COLOR::BLUE:
	{
		color = Color3B::BLUE;
		break;
	}
	case COLOR::YELLOW:
	{
		color = Color3B::YELLOW;
		break;
	}
	case COLOR::GREEN:
	{
		color = Color3B::GREEN;
		break;
	}
	case COLOR::PURPLE:
	{
		color = Color3B::MAGENTA;
		break;
	}
	default:
		break;
	}
	return color;
}


std::vector<Vec2> Board::chooseListBaseColor(COLOR inColor)
{
	switch (inColor)
	{
	case RED:
		return listRedPipe;
		break;
	case YELLOW:
		return listYellowPipe;
		break;
	case GREEN:
		return listGreenPipe;
		break;
	case BLUE:
		return listBluePipe;
		break;
	case PURPLE:
		return listPurplePipe;
		break;
	default:
		break;
	}
}
void Board::clearAllListColor()
{
	this->getListBluePipe().clear();
	this->getListGreenPipe().clear();
	this->getListRedPipe().clear();
	this->getListPurplePipe().clear();
	this->getListYellowPipe().clear();
}
void Board::copyListBaseColor(COLOR inColor,std::vector<Vec2> listVec2)
{
	switch (inColor)
	{
	case RED:
		listRedPipe = listVec2;
		break;
	case YELLOW:
		listYellowPipe = listVec2;
		break;
	case GREEN:
		listGreenPipe = listVec2;
		break;
	case BLUE:
		listBluePipe = listVec2;
		break;
	case PURPLE:
		listPurplePipe = listVec2;
		break;
	default:
		break;
	}
}
bool Board::isNeighbour(int baseCol, int baseRow, int nextCol, int nextRow)
{
	int deltaX = baseCol - nextCol;
	int deltaY = baseRow - nextRow;
	//touch go down
	if (deltaX == 0 && deltaY == -1)
	{
		return true;
	}
	//touch go down
	else if (deltaX == 0 && deltaY == 1)
	{
		return true;
	}
	else if (baseCol % 2 == 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == 0)
		{
			return true;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 1)
		{
			return true;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == 0)
		{
			return true;
		}
		//touch go bot right
		else if (deltaX == -1 && deltaY == 1 )
		{
			return true;
		}
	}
	else if (baseCol % 2 != 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == -1)
		{
			return true;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 0)
		{
			return true;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == -1)
		{
			return true;
		}
		//touch go bot right
		else if (deltaX == -1 && deltaY == 0)
		{
			return true;
		}
	}
	return false;
}
int Board::checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList)
{
	for (int i = 0; i < newList.size(); i++)
	{
		if ((int)newValue.x == (int)newList.at(i).x && (int)newValue.y == (int)newList.at(i).y)
		{
			return i;
		}
	}
	return -1;
}
void Board::deleteAllPipeSameColor(COLOR color)
{
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (m_pipesMatrix[i][j] && m_pipesMatrix[i][j]->getColor() == color)
			{
				if (m_holesMatrix[i][j])
				{
					m_holesMatrix[i][j]->setChecked(false);
				}
				m_pipesMatrix[i][j]->removeFromParentAndCleanup(true);
				m_pipesMatrix[i][j] = nullptr;
				m_emptyMatrix[i][j]->setColor(Color3B::WHITE);
			}
		}
	}
}
void Board::drawPipeOnList(std::vector<cocos2d::Vec2> listVec2)
{
	for (int i = 0; i < listVec2.size() ;i++)
	{
		if ((i+1) < listVec2.size())
		{
			//has hole and not check
			if (m_holesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y])
			{
				if (!m_holesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y]->getChecked())
				{
					choosePipeAndCreateOnHole(listVec2.at(i), listVec2.at(i + 1));
					//m_holesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y]->setChecked(true);
				}
			}
			//not has hole 
			else
			{
				//has pipe in the last touch cell 
				if ((m_pipesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y]) && !(m_pipesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y]->getChecked()))
				{
					choosePipeAndCreate(listVec2.at(i), listVec2.at(i + 1),listVec2);
				}
			}
		}
		else
		{
			break;
		}
	}
}
void Board::choosePipeAndCreateOnHole(Vec2 prePos, Vec2 nextPos)
{
	COLOR colorCurrent = GameManager::getInstance()->getColorCurrent();
	int preCol = (int)prePos.x;
	int preRow = (int)prePos.y;
	int nextCol = (int)nextPos.x;
	int nextRow = (int)nextPos.y;
	int deltaX = preCol - nextCol;
	int deltaY = preRow - nextRow;
	int prePipeRotation = 0;
	int nextPipeRotation = 0;
	DIR newLastDirection;
	//touch go up
	if (deltaX == 0 && deltaY == -1)
	{
		newLastDirection = TOP;
		prePipeRotation = 0;
		nextPipeRotation = -180;
	}
	//touch go down
	else if (deltaX == 0 && deltaY == 1)
	{
		newLastDirection = DOWN;
		prePipeRotation = 180;
		nextPipeRotation = 0;
	}
	else if (preCol % 2 == 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == 0)
		{
			newLastDirection = TOP_LEFT;
			prePipeRotation = 300;
			nextPipeRotation = 120;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 1)
		{
			newLastDirection = BOT_LEFT;
			prePipeRotation = 240;
			nextPipeRotation = 60;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == 0)
		{
			newLastDirection = TOP_RIGHT;
			prePipeRotation = 60;
			nextPipeRotation = 240;
		}
		//touch go bot right
		else
		{
			newLastDirection = BOT_RIGHT;
			prePipeRotation = 120;
			nextPipeRotation = 300;
		}
	}
	else if (preCol % 2 != 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == -1)
		{
			newLastDirection = TOP_LEFT;
			prePipeRotation = 300;
			nextPipeRotation = 120;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 0)
		{
			newLastDirection = BOT_LEFT;
			prePipeRotation = 240;
			nextPipeRotation = 60;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == -1)
		{
			newLastDirection = TOP_RIGHT;
			prePipeRotation = 60;
			nextPipeRotation = 240;
		}
		//touch go bot right
		else
		{
			newLastDirection = BOT_RIGHT;
			prePipeRotation = 120;
			nextPipeRotation = 300;
		}
	}
	GameManager::getInstance()->setLastDirection(newLastDirection);
	Pipe *pre_pipe;
	pre_pipe = new Pipe(END, colorCurrent, preCol, preRow);
	pre_pipe->setRotation(prePipeRotation);
	pre_pipe->setDirection(newLastDirection);
	pre_pipe->setPosition(getEmptyMatrix()[preCol][preRow]->getPosition());
	m_pipesMatrix[preCol][preRow] = pre_pipe;
	m_layer->addChild(m_pipesMatrix[preCol][preRow], PIPEMATRIX);

	if (!m_holesMatrix[nextCol][nextRow] && m_pipesMatrix[nextCol][nextRow])
	{
		m_emptyMatrix[nextCol][nextRow]->setColor(Color3B::WHITE);
		m_pipesMatrix[nextCol][nextRow]->removeFromParentAndCleanup(true);
		m_pipesMatrix[nextCol][nextRow] = nullptr;
	}
	Pipe *next_pipe;
	next_pipe = new Pipe(END, colorCurrent, nextCol, nextRow);
	next_pipe->setRotation(nextPipeRotation);
	next_pipe->setPosition(getEmptyMatrix()[nextCol][nextRow]->getPosition());
	m_pipesMatrix[nextCol][nextRow] = next_pipe;
	m_layer->addChild(m_pipesMatrix[nextCol][nextRow], PIPEMATRIX);

}
void Board::choosePipeAndCreate(Vec2 prePos, Vec2 nextPos, std::vector<cocos2d::Vec2> listVec2)
{
	COLOR colorCurrent = GameManager::getInstance()->getColorCurrent();
	int preCol = (int)prePos.x;
	int preRow = (int)prePos.y;
	int nextCol = (int)nextPos.x;
	int nextRow = (int)nextPos.y;
	int deltaX = preCol - nextCol;
	int deltaY = preRow - nextRow;
	DIR newNextDirection;
	//touch go up
	if (deltaX == 0 && deltaY == -1)
	{
		newNextDirection = TOP;
	}
	//touch go down
	else if (deltaX == 0 && deltaY == 1)
	{
		newNextDirection = DOWN;
	}
	else if (preCol % 2 == 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == 0)
		{
			newNextDirection = TOP_LEFT;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 1)
		{
			newNextDirection = BOT_LEFT;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == 0)
		{
			newNextDirection = TOP_RIGHT;
		}
		//touch go bot right
		else
		{
			newNextDirection = BOT_RIGHT;
		}
	}
	else if (preCol % 2 != 0)
	{
		//touch go top left
		if (deltaX == 1 && deltaY == -1)
		{
			newNextDirection = TOP_LEFT;
		}
		//touch go bot left
		else if (deltaX == 1 && deltaY == 0)
		{
			newNextDirection = BOT_LEFT;
		}
		//touch go top right
		else if (deltaX == -1 && deltaY == -1)
		{
			newNextDirection = TOP_RIGHT;
		}
		//touch go bot right
		else
		{
			newNextDirection = BOT_RIGHT;
		}
	}
	GameManager::getInstance()->setNextDirection(newNextDirection);
	choosePipeAndCreateAtCurvePoint();
	int curvePipeRotation = GameManager::getInstance()->getPipeCurveRotation();
	int nextPipeRotation = GameManager::getInstance()->getNextPipeCurveRotation();
	TYPEPIPE curveTypePipe = GameManager::getInstance()->getTypePipeCurve();
	if (!m_holesMatrix[preCol][preRow] && m_pipesMatrix[preCol][preRow])
	{
		m_emptyMatrix[preCol][preRow]->setColor(Color3B::WHITE);
		m_pipesMatrix[preCol][preRow]->removeFromParentAndCleanup(true);
		m_pipesMatrix[preCol][preRow] = nullptr;
	}
	Pipe *curvePipe;
	curvePipe = new Pipe(curveTypePipe, colorCurrent, preCol, preRow);
	curvePipe->setRotation(curvePipeRotation);
	curvePipe->setDirection(newNextDirection);
	curvePipe->setPosition(getEmptyMatrix()[preCol][preRow]->getPosition());
	m_pipesMatrix[preCol][preRow] = curvePipe;
	m_layer->addChild(m_pipesMatrix[preCol][preRow], PIPEMATRIX);

	if (!m_holesMatrix[nextCol][nextRow] && m_pipesMatrix[nextCol][nextRow])
	{
		m_emptyMatrix[nextCol][nextRow]->setColor(Color3B::WHITE);
		m_pipesMatrix[nextCol][nextRow]->removeFromParentAndCleanup(true);
		m_pipesMatrix[nextCol][nextRow] = nullptr;
	}
	Pipe *nextPipe;
	nextPipe = new Pipe(END, colorCurrent, nextCol, nextRow);
	nextPipe->setRotation(nextPipeRotation);
	nextPipe->setPosition(getEmptyMatrix()[nextCol][nextRow]->getPosition());
	m_pipesMatrix[nextCol][nextRow] = nextPipe;
	m_layer->addChild(m_pipesMatrix[nextCol][nextRow], PIPEMATRIX);

	GameManager::getInstance()->setLastDirection(GameManager::getInstance()->getNextDirection());
}
void Board::choosePipeAndCreateAtCurvePoint()
{
	DIR lastDir = GameManager::getInstance()->getLastDirection();
	DIR nextDir = GameManager::getInstance()->getNextDirection();
	TYPEPIPE typePipe;
	int curveRotation;
	int nextRotation;
	if (lastDir == TOP)
	{
		if (nextDir == TOP)
		{
			typePipe = STRAIGHT;
			curveRotation = 180;
			nextRotation = 180;
		}
		else if (nextDir == TOP_LEFT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 180;
			nextRotation = 120;
		}
		else if (nextDir == BOT_LEFT)
		{ 
			typePipe = V_PIPE_60;
			curveRotation = 180;
			nextRotation = 60;
		}
		else if (nextDir == TOP_RIGHT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 60;
			nextRotation = 240;
		}
		else
		{
			typePipe = V_PIPE_60;
			curveRotation = 120;
			nextRotation = 300;
		}
	}
	else if (lastDir == DOWN)
	{
		if (nextDir == DOWN)
		{
			typePipe = STRAIGHT;
			curveRotation = 0;
			nextRotation = 0;
		}
		else if (nextDir == TOP_LEFT)
		{
			typePipe = V_PIPE_60;
			curveRotation = 300;
			nextRotation = 120;
		}
		else if (nextDir == BOT_LEFT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 240;
			nextRotation = 60;
		}
		else if (nextDir == TOP_RIGHT)
		{
			typePipe = V_PIPE_60;
			curveRotation = 0;
			nextRotation = 240;
		}
		else
		{
			typePipe = V_PIPE_120;
			curveRotation = 0;
			nextRotation = 300;
		}
	}
	else if (lastDir == TOP_LEFT)
	{
		if (nextDir == TOP)
		{
			typePipe = V_PIPE_120;
			curveRotation = 0;
			nextRotation = 180;
		}
		else if (nextDir == DOWN)
		{
			typePipe = V_PIPE_60;
			curveRotation = 120;
			nextRotation = 0;
		}
		else if (nextDir == TOP_LEFT)
		{
			typePipe = STRAIGHT;
			curveRotation = 120;
			nextRotation = 120;
		}
		else if (nextDir == BOT_LEFT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 120;
			nextRotation = 60;
		}
		else if (nextDir == TOP_RIGHT)
		{
			typePipe = V_PIPE_60;
			curveRotation = 60;
			nextRotation = 240;
		}
	}
	else if (lastDir == BOT_LEFT)
	{
		if (nextDir == TOP)
		{
			typePipe = V_PIPE_60;
			curveRotation = 0;
			nextRotation = 180;
		}
		else if (nextDir == DOWN)
		{
			typePipe = V_PIPE_120;
			curveRotation = 60;
			nextRotation = 0;
		}
		else if (nextDir == TOP_LEFT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 300;
			nextRotation = 120;
		}
		else if (nextDir == BOT_LEFT)
		{
			typePipe = STRAIGHT;
			curveRotation = 60;
			nextRotation = 60;
		}
		else
		{
			typePipe = V_PIPE_60;
			curveRotation = 60;
			nextRotation = 300;
		}
	}
	else if (lastDir == TOP_RIGHT)
	{
		if (nextDir == TOP)
		{
			typePipe = V_PIPE_120;
			curveRotation = 240;
			nextRotation = 180;
		}
		else if (nextDir == DOWN)
		{
			typePipe = V_PIPE_60;
			curveRotation = 180;
			nextRotation = 0;
		}
		else if (nextDir == TOP_LEFT)
		{
			typePipe = V_PIPE_60;
			curveRotation = 240;
			nextRotation = 120;
		}
		else if (nextDir == TOP_RIGHT)
		{
			typePipe = STRAIGHT;
			curveRotation = 240;
			nextRotation = 240; 
		}
		else
		{
			typePipe = V_PIPE_120;
			curveRotation = 120;
			nextRotation = 300;
		}
	}
	else
	{
		if (nextDir == TOP)
		{
			typePipe = V_PIPE_60;
			curveRotation = 300;
			nextRotation = 180;
		}
		else if (nextDir == DOWN)
		{
			typePipe = V_PIPE_120;
			curveRotation = 180;
			nextRotation = 0;
		}
		else if (nextDir == BOT_LEFT)
		{
			typePipe = V_PIPE_60;
			curveRotation = 240;
			nextRotation = 60;
		}
		else if (nextDir == TOP_RIGHT)
		{
			typePipe = V_PIPE_120;
			curveRotation = 300;
			nextRotation = 240;
		}
		else
		{
			typePipe = STRAIGHT;
			curveRotation = 300;
			nextRotation = 300;
		}
	}
	GameManager::getInstance()->setTypePipeCurve(typePipe);
	GameManager::getInstance()->setPipeCurveRotation(curveRotation);
	GameManager::getInstance()->setNextPipeCurveRotation(nextRotation);
}
int Board::getRotationForGoBackWard(DIR preDirection)
{
	int rotation = 0;
	if (preDirection == TOP)
	{
		rotation = 180;
	}
	else if (preDirection == DOWN)
	{
		rotation = 0;
	}
	else if (preDirection == TOP_LEFT)
	{
		rotation = 120;
	}
	else if (preDirection == BOT_LEFT)
	{
		rotation = 60;
	}
	else if (preDirection == TOP_RIGHT)
	{
		rotation = 240;
	}
	else
	{
		rotation = 300;
	}
	return rotation;
}

void Board::showHint()
{
	for (int i = 0;i < widthBoard ;i++)
	{
		for (int j = 0;j<heightBoard;j++)
		{

		}
	}
}

void Board::markBoardByColor(std::vector<Vec2> listVec2)
{
	COLOR color = GameManager::getInstance()->getColorCurrent();
	for (int i = 0 ; i < listVec2.size();i++)
	{
		if (m_pipesMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y])
		{
			m_emptyMatrix[(int)listVec2.at(i).x][(int)listVec2.at(i).y]->setColor(chooseColorToMark(color));
		}
	}
}
cocos2d::Color3B Board::chooseColorToMark(COLOR incolor)
{
	Color3B color;
	switch (incolor)
	{
	case COLOR::RED:
	{
		color = Color3B::RED;
		break;
	}
	case COLOR::BLUE:
	{
		color = Color3B::BLUE;
		break;
	}
	case COLOR::YELLOW:
	{
		color = Color3B::YELLOW;
		break;
	}
	case COLOR::GREEN:
	{
		color = Color3B::GREEN;
		break;
	}
	case COLOR::PURPLE:
	{
		color = Color3B::MAGENTA;
		break;
	}
	default:
		break;
	}
	return color;
}

float Board::calculatePercentFillBoard()
{
	float countBoard = 0;
	float countPipe = 0;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (m_emptyMatrix[i][j])
			{
				countBoard++;
				if (m_pipesMatrix[i][j])
				{
					countPipe++;
				}
			}
		}
	}
	float percent = (float)countPipe / countBoard *100.0f;
	return percent;
}

int Board::randomRange(int min, int max)
{
	int r = rand() % (max - min + 1) + min;
	return r;
}


Sprite*** Board::getEmptyMatrix()
{
	return m_emptyMatrix;
}


Pipe*** Board::getHolesMatrix()
{
	return m_holesMatrix;
}


Pipe*** Board::getPipesMatrix()
{
	return m_pipesMatrix;
}

Point Board::getPositionHexa(int i, int j)
{
	Point p;
	/*float xOFFSET = 0.882f;
	float yOFFSET = 0.764f;*/
	/*float xOFFSET =	1.0f;
	float yOFFSET = 1.0f;*/

	//size 9x11 x/5.5
	//size 6x8  x/4.0
	//size 7x9  x/4.2
	//size 5x6  x/4.5
	float xPos;
	float yPos;
	switch (GameManager::getInstance()->getWidth())
	{
		case 5:
		{
			xPos = (i + widthBoard / 4.5)*OFFSET_Y;
			yPos = (j + heightBoard / 2.2)*OFFSET_X;
			break;
		}
		case 6:
		{
			xPos = (i + widthBoard / 4.0)*OFFSET_Y;
			yPos = (j + heightBoard / 2.5)*OFFSET_X;
			break;
		}
		case 7:
		{
			xPos = (i + widthBoard / 4.2)*OFFSET_Y;
			yPos = (j + heightBoard / 2.4)*OFFSET_X;
			break;
		}
		case 9:
		{
			xPos = (i + widthBoard / 5.5)*OFFSET_Y;
			yPos = (j + heightBoard / 2.6)*OFFSET_X;
			break;
		}
	default:
		break;
	}
	if (i % 2 == 1)
	{
		yPos += OFFSET_X / 2;
	}
	p = Point(xPos, yPos);
	return  p;
}

int Board::getPreTouchCol()
{
	return preTouch_col;
}

void Board::setPreTouchCol(int newValue)
{
	preTouch_col = newValue;
}

int Board::getPreTouchRow()
{
	return preTouch_row;
}

void Board::setPreTouchRow(int newValue)
{
	preTouch_row = newValue;
}

int Board::getNextTouchCol()
{
	return nextTouch_col;
}

void Board::setNextTouchCol(int newValue)
{
	nextTouch_col = newValue;
}

int Board::getNextTouchRow()
{
	return nextTouch_row;
}

void Board::setNextTouchRow(int newValue)
{
	nextTouch_row = newValue;
}

bool Board::getMeetSameHole()
{
	return meetSameHole;
}

void Board::setMeetSameHole(bool newValue)
{
	meetSameHole = newValue;
}


void Board::resetTouchPos()
{
	setPreTouchCol(0);
	setPreTouchRow(0);
	setNextTouchCol(0);
	setNextTouchRow(0);
}

std::vector<Vec2> Board::getListRedPipe()
{
	return listRedPipe;
}

std::vector<Vec2> Board::getListBluePipe()
{
	return listBluePipe;
}

std::vector<Vec2> Board::getListGreenPipe()
{
	return listGreenPipe;
}

std::vector<Vec2> Board::getListYellowPipe()
{
	return listYellowPipe;
}

std::vector<Vec2> Board::getListPurplePipe()
{
	return listPurplePipe;
}

