#include "EditorScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

cocos2d::Scene* EditorScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = EditorScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool EditorScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(192, 192, 192, 255)))
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	currentEditColor = RED;
	currentEditPackage = 1;
	isEditingBoard = true;
	isEditingBoardVisible = true;
	isEditingHole = false;
	isEditingHint = false;
	widthBoard = GameManager::getInstance()->getWidth();
	heightBoard = GameManager::getInstance()->getHeight();
	scaleBoard = GameManager::getInstance()->getScaleBoard();
	scalePipe = GameManager::getInstance()->getScalePipe();
	initBoard();
	initButton();
	touchEvent();
	
	return true;
}

void EditorScene::initBoard()
{
	if (GameManager::getInstance()->getLoadLevel())
	{
		board = new Board(this, true);
	}
	else
	{
		board = new Board(this, false);
	}
}

void EditorScene::initButton()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	btn_SaveLevel = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::saveLevel, this));
	btn_SaveLevel->setScale(0.9f);
	btn_SaveLevel->setColor(Color3B::BLACK);
	btn_SaveLevel->setPosition(WIN_SIZE.width / 2 + WIN_SIZE.width / 2.7,WIN_SIZE.height / 2 + WIN_SIZE.height / 3);

	btn_LoadLevel = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::loadLevel, this));
	btn_LoadLevel->setScale(0.9f);
	btn_LoadLevel->setColor(Color3B::BLACK);
	btn_LoadLevel->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 100);

	btn_Package = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::changePackage, this));
	btn_Package->setScale(0.9f);
	btn_Package->setColor(Color3B::BLACK);
	btn_Package->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 200);

	btn_ClearHole = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::clearHole, this));
	btn_ClearHole->setScale(0.9f);
	btn_ClearHole->setColor(Color3B::BLACK);
	btn_ClearHole->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 300);

	btn_ClearBoard = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::clearBoard, this));
	btn_ClearBoard->setScale(0.9f);
	btn_ClearBoard->setColor(Color3B::BLACK);
	btn_ClearBoard->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 400);

	btn_HoleColor = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::changeHoleColor, this));
	btn_HoleColor->setScale(0.9f);
	btn_HoleColor->setColor(Color3B::RED);
	btn_HoleColor->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 500);

	btn_SizeBoard = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::changeSizeBoard, this));
	btn_SizeBoard->setScale(0.9f);
	btn_SizeBoard->setColor(Color3B::BLACK);
	btn_SizeBoard->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 600);

	btn_EditBoard = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::editBoard, this));
	btn_EditBoard->setScale(0.9f);
	btn_EditBoard->setColor(Color3B::GREEN);
	btn_EditBoard->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 700);

	btn_EditHole = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::editHole, this));
	btn_EditHole->setScale(0.9f);
	btn_EditHole->setColor(Color3B::BLACK);
	btn_EditHole->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 800);

	btn_EditHint = MenuItemImage::create(BTN, BTN, CC_CALLBACK_1(EditorScene::editHint, this));
	btn_EditHint->setScale(0.9f);
	btn_EditHint->setColor(Color3B::BLACK);
	btn_EditHint->setPosition(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y - 900);

	menu = Menu::create(btn_ClearBoard, btn_ClearHole, btn_SaveLevel, btn_EditBoard, btn_EditHole, btn_HoleColor,btn_EditHint,btn_LoadLevel,btn_Package,btn_SizeBoard, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, UI);

	clearBoardLabel = Label::createWithTTF("CLEAR BOARD", "fonts/impact.ttf", 40);
	clearBoardLabel->setColor(Color3B::WHITE);
	clearBoardLabel->setPosition(btn_ClearBoard->getPosition());
	this->addChild(clearBoardLabel,UI + 1);

	changePackageLabel = Label::createWithTTF("NOVICE", "fonts/impact.ttf", 40);
	changePackageLabel->setColor(Color3B::WHITE);
	changePackageLabel->setPosition(btn_Package->getPosition());
	this->addChild(changePackageLabel, UI + 1);

	loadLevelLabel = Label::createWithTTF("LOAD LEVEL", "fonts/impact.ttf", 40);
	loadLevelLabel->setColor(Color3B::WHITE);
	loadLevelLabel->setPosition(btn_LoadLevel->getPosition());
	this->addChild(loadLevelLabel, UI + 1);

	clearHoleLabel = Label::createWithTTF("CLEAR HOLE", "fonts/impact.ttf", 40);
	clearHoleLabel->setColor(Color3B::WHITE);
	clearHoleLabel->setPosition(btn_ClearHole->getPosition());
	this->addChild(clearHoleLabel, UI + 1);

	saveLabel = Label::createWithTTF("SAVE LEVEL", "fonts/impact.ttf", 40);
	saveLabel->setPosition(btn_SaveLevel->getPosition());
	this->addChild(saveLabel,UI + 1);

	holeColorLabel = Label::createWithTTF("HOLE COLOR", "fonts/impact.ttf", 40);
	holeColorLabel->setPosition(btn_HoleColor->getPosition());
	this->addChild(holeColorLabel,UI + 1);

	sizeBoardLabel = Label::createWithTTF("SIZE BOARD", "fonts/impact.ttf", 40);
	sizeBoardLabel->setPosition(btn_SizeBoard->getPosition());
	this->addChild(sizeBoardLabel, UI + 1);

	editBoardLabel = Label::createWithTTF("EDIT BOARD", "fonts/impact.ttf", 40);
	editBoardLabel->setPosition(btn_EditBoard->getPosition());
	this->addChild(editBoardLabel,UI + 1);

	editHoleLabel = Label::createWithTTF("EDIT HOLE", "fonts/impact.ttf", 40);
	editHoleLabel->setPosition(btn_EditHole->getPosition());
	this->addChild(editHoleLabel,UI + 1);

	editHintLabel = Label::createWithTTF("EDIT HINT", "fonts/impact.ttf", 40);
	editHintLabel->setPosition(btn_EditHint->getPosition());
	this->addChild(editHintLabel, UI + 1);

	txt = cocos2d::ui::TextField::create("lv_X.sav", "fonts/impact.ttf", 40);
	txt->setTextHorizontalAlignment(TextHAlignment::CENTER);
	txt->setTextVerticalAlignment(TextVAlignment::CENTER);
	
	txt->setPosition((Vec2(btn_SaveLevel->getPosition().x, btn_SaveLevel->getPosition().y + 100)));
	txt->setMaxLength(20);
	txt->setMaxLengthEnabled(true);
	txt->setTouchSize(Size(200, 100));
	this->addChild(txt, UI);
}
void EditorScene::touchEvent()
{
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(EditorScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(EditorScene::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(EditorScene::onTouchEnded, this);
	listener->onTouchCancelled = CC_CALLBACK_2(EditorScene::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	this->setTouchEnabled(true);
}

void EditorScene::changeHoleColor(cocos2d::Ref *sender)
{
	if (currentEditColor == NONECOLOR)
	{
		currentEditColor = RED;
		btn_HoleColor->setColor(Color3B::RED);
	}
	else if (currentEditColor == RED)
	{
		currentEditColor = YELLOW;
		btn_HoleColor->setColor(Color3B::YELLOW);
	}
	else if (currentEditColor == YELLOW)
	{
		currentEditColor = GREEN;
		btn_HoleColor->setColor(Color3B::GREEN);
	}
	else if (currentEditColor == GREEN)
	{
		currentEditColor = BLUE;
		btn_HoleColor->setColor(Color3B::BLUE);
	}
	else if (currentEditColor == BLUE)
	{
		currentEditColor = PURPLE;
		btn_HoleColor->setColor(Color3B::MAGENTA);
	}
	else if (currentEditColor == PURPLE)
	{
		currentEditColor = RED;
		btn_HoleColor->setColor(Color3B::RED);
	}
}

void EditorScene::editBoard(cocos2d::Ref *sender)
{
	isEditingHole = false;
	isEditingHint = false;
	isEditingBoard = true;
	btn_EditHint->setColor(Color3B::BLACK);
	btn_EditHole->setColor(Color3B::BLACK);
	btn_EditBoard->setColor(Color3B::GREEN);
}

void EditorScene::editHole(cocos2d::Ref *sender)
{
	isEditingHint = false;
	isEditingBoard = false;
	isEditingHole = true;
	btn_EditHint->setColor(Color3B::BLACK);
	btn_EditBoard->setColor(Color3B::BLACK);
	btn_EditHole->setColor(Color3B::GREEN);
}

void EditorScene::editHint(cocos2d::Ref *sender)
{
	isEditingHole = false;
	isEditingBoard = false;
	isEditingHint = true;
	btn_EditHole->setColor(Color3B::BLACK);
	btn_EditBoard->setColor(Color3B::BLACK);
	btn_EditHint->setColor(Color3B::GREEN);
}

void EditorScene::saveLevel(cocos2d::Ref *sender)
{
	int countHole = 0;
	LevelState levelState;
	levelState.width = widthBoard;
	levelState.height = heightBoard;
	levelState.scaleBoard = scaleBoard;
	levelState.scalePipe = scalePipe;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j]->isVisible())
			{
				levelState.board[i][j] = 1;
				if (board->getEmptyMatrix()[i][j]->getColor() != Color3B::WHITE)
				{
					saveHintToList(i, j);
				}
				Pipe *hole = board->getHolesMatrix()[i][j];
				if (hole)
				{
					levelState.hole_Color[i][j] = hole->getColor();
					countHole++;
				}
				else
				{
					levelState.hole_Color[i][j] = 0;
				}
			}
			else
			{
				levelState.board[i][j] = 0;
			}
		}
	}
	saveHint(levelState);
	levelState.maxMove = countHole / 2;
	levelState.bestMove = 0;
	levelState.starEarned = 0;
	GameManager::getInstance()->setLevelState(levelState);
	UserData::getInstance()->setNameLevelFileToSave(StringUtils::format("lv_%s.sav", txt->getString().c_str()));
	UserData::getInstance()->saveLevelIntoFile();
}

void EditorScene::loadLevel(cocos2d::Ref *sender)
{
	if (txt->getString().c_str() != "")
	{
		UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%s.sav",txt->getString().c_str()));
		UserData::getInstance()->loadLevelFromFile();
		GameManager::getInstance()->setLoadLevel(true);
		GameManager::getInstance()->setWidth(GameManager::getInstance()->getLevelState().width);
		GameManager::getInstance()->setHeight(GameManager::getInstance()->getLevelState().height);
		GameManager::getInstance()->setScaleBoard(GameManager::getInstance()->getLevelState().scaleBoard);
		GameManager::getInstance()->setScalePipe(GameManager::getInstance()->getLevelState().scalePipe);
		auto scene = EditorScene::createScene();
		Director::getInstance()->replaceScene(TransitionCrossFade::create(0.7f, scene));
	}
}

void EditorScene::changeSizeBoard(cocos2d::Ref *sender)
{
	switch (GameManager::getInstance()->getWidth())
	{
		case 5:
		{
			GameManager::getInstance()->setWidth(6);
			GameManager::getInstance()->setHeight(8);
			GameManager::getInstance()->setScaleBoard(0.35f);
			GameManager::getInstance()->setScalePipe(0.35f);
			break;
		}
		case 6:
		{
			GameManager::getInstance()->setWidth(7);
			GameManager::getInstance()->setHeight(9);
			GameManager::getInstance()->setScaleBoard(0.30f);
			GameManager::getInstance()->setScalePipe(0.30f);
			break;
		}
		case 7:
		{
			GameManager::getInstance()->setWidth(9);
			GameManager::getInstance()->setHeight(11);
			GameManager::getInstance()->setScaleBoard(0.25f);
			GameManager::getInstance()->setScalePipe(0.25f);
			break;
		}
		case 9:
		{
			GameManager::getInstance()->setWidth(5);
			GameManager::getInstance()->setHeight(6);
			GameManager::getInstance()->setScaleBoard(0.45f);
			GameManager::getInstance()->setScalePipe(0.45f);
			break;
		}
	default:
		break;
	}
	auto scene = EditorScene::createScene();
	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.7f, scene));
}

void EditorScene::changePackage(cocos2d::Ref *sender)
{
	switch (currentEditPackage)
	{
		case 1:
		{
			currentEditPackage = 2;
			GameManager::getInstance()->setCurrentPackage(currentEditPackage);
			changePackageLabel->setString("BEGINNER");
			break;
		}
		case 2:
		{
			currentEditPackage = 3;
			GameManager::getInstance()->setCurrentPackage(currentEditPackage);
			changePackageLabel->setString("ADVANCE");
			break;
		}
		case 3:
		{
			currentEditPackage = 4;
			GameManager::getInstance()->setCurrentPackage(currentEditPackage);
			changePackageLabel->setString("EXPERT");
			break;
		}
		case 4:
		{
			currentEditPackage = 5;
			GameManager::getInstance()->setCurrentPackage(currentEditPackage);
			changePackageLabel->setString("MASTER");
			break;
		}
		case 5:
		{
			currentEditPackage = 1;
			GameManager::getInstance()->setCurrentPackage(currentEditPackage);
			changePackageLabel->setString("NOVICE");
			break;
		}
	default:
		break;
	}
}
int EditorScene::checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList)
{
	for (int i = 0; i < newList.size(); i++)
	{
		if ((int)newValue.x == (int)newList.at(i).x && (int)newValue.y == (int)newList.at(i).y)
		{
			return i;
		}
	}
	return -1;
}
void EditorScene::createHoleOnBoard(int col, int row)
{
	Pipe *hole = new Pipe(HOLE, currentEditColor, col, row);
	hole->setPosition(board->getEmptyMatrix()[col][row]->getPosition());
	board->getHolesMatrix()[col][row] = hole;
	this->addChild(board->getHolesMatrix()[col][row], HOLEMATRIX);
}
void EditorScene::saveHintToList(int col,int row)
{
	Vec2 index = Vec2(col, row);
	
	switch (getColorFromEmptyMatrix(col, row))
	{
	case 1:
	{
		if (checkVec2InList(index, listRedHint) == -1)
		{
			listRedHint.push_back(index);
		}
		break;
	}
	case 2:
	{
		if (checkVec2InList(index, listYellowHint) == -1)
		{
			listYellowHint.push_back(index);
		}
		break;
	}
	case 3:
	{
		if (checkVec2InList(index, listGreenHint) == -1)
		{
			listGreenHint.push_back(index);
		}
		break;
	}
	case 4:
	{
		if (checkVec2InList(index, listBlueHint) == -1)
		{
			listBlueHint.push_back(index);
		}
		break;
	}
	case 5:
	{
		if (checkVec2InList(index, listPurpleHint) == -1)
		{
			listPurpleHint.push_back(index);
		}
		break;
	}
	default:
		break;
	}
}
void EditorScene::saveHint(LevelState &levelState)
{
	for (int i = 0; i < 50;i++)
	{
		if (i < listRedHint.size())
		{
			levelState.redHint[i].x = listRedHint.at(i).x;
			levelState.redHint[i].y = listRedHint.at(i).y;
		}
		else
		{
			levelState.redHint[i].x = -1;
			levelState.redHint[i].y = -1;
		}
	}

	for (int i = 0; i < 50; i++)
	{
		if (i < listBlueHint.size())		
		{
			levelState.blueHint[i].x = listBlueHint.at(i).x;
			levelState.blueHint[i].y = listBlueHint.at(i).y;
		}
		else
		{
			levelState.blueHint[i].x = -1;
			levelState.blueHint[i].y = -1;
		}
	}

	for (int i = 0; i < 50; i++)
	{
		if (i < listGreenHint.size())
		{
			levelState.greenHint[i].x = listGreenHint.at(i).x;
			levelState.greenHint[i].y = listGreenHint.at(i).y;
		}
		else
		{
			levelState.greenHint[i].x = -1;
			levelState.greenHint[i].y = -1;
		}
	}

	for (int i = 0; i < 50; i++)
	{
		if (i < listPurpleHint.size())
		{
			levelState.purpleHint[i].x = listPurpleHint.at(i).x;
			levelState.purpleHint[i].y = listPurpleHint.at(i).y;
		}
		else
		{
			levelState.purpleHint[i].x = -1;
			levelState.purpleHint[i].y = -1;
		}
	}

	for (int i = 0; i < 50; i++)
	{
		if (i < listYellowHint.size())
		{
			levelState.yellowHint[i].x = listYellowHint.at(i).x;
			levelState.yellowHint[i].y = listYellowHint.at(i).y;
		}
		else
		{
			levelState.yellowHint[i].x = -1;
			levelState.yellowHint[i].y = -1;
		}
	}
}

int EditorScene::getColorFromEmptyMatrix(int col, int row)
{
	Color3B color = board->getEmptyMatrix()[col][row]->getColor();
	if (color == Color3B::RED)
	{
		return 1;
	}
	else if (color == Color3B::YELLOW)
	{
		return 2;
	}
	else if (color == Color3B::GREEN)
	{
		return 3;
	}
	else if (color == Color3B::BLUE)
	{
		return 4;
	}
	else if (color == Color3B::MAGENTA)
	{
		return 5;
	}
	return 0;
}
void EditorScene::clearHole(Ref *ref)
{
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getHolesMatrix()[i][j])
			{
				this->removeChild(board->getHolesMatrix()[i][j]);
				board->getHolesMatrix()[i][j] = nullptr;
			}
			board->getEmptyMatrix()[i][j]->setColor(Color3B::WHITE);
		}
	}
} 
void EditorScene::clearBoard(Ref *ref)
{
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getHolesMatrix()[i][j])
			{	
				this->removeChild(board->getHolesMatrix()[i][j]);
				board->getHolesMatrix()[i][j] = nullptr;
			}
			board->getEmptyMatrix()[i][j]->setColor(Color3B::WHITE);
			board->getEmptyMatrix()[i][j]->setVisible(true);
		}
	}
}
void EditorScene::colorEmptyMatrix(int col, int row)
{
	switch (currentEditColor)
	{
		case RED:
			board->getEmptyMatrix()[col][row]->setColor(Color3B::RED);
			break;
		case GREEN:
			board->getEmptyMatrix()[col][row]->setColor(Color3B::GREEN);
			break;
		case BLUE:
			board->getEmptyMatrix()[col][row]->setColor(Color3B::BLUE);
			break;
		case PURPLE:
			board->getEmptyMatrix()[col][row]->setColor(Color3B::MAGENTA);
			break;
		case YELLOW:
			board->getEmptyMatrix()[col][row]->setColor(Color3B::YELLOW);
			break;
		default:
			break;
	}
}

bool EditorScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	int sz = Sprite::create(BOARD)->getContentSize().width*scalePipe;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j]->getPosition().distance(touch->getLocation()) < sz / 2)
			{
				if (isEditingBoard)
				{
					if (board->getEmptyMatrix()[i][j]->isVisible())
					{
						board->getEmptyMatrix()[i][j]->setVisible(false);
						isEditingBoardVisible = true;
					}
					else
					{
						board->getEmptyMatrix()[i][j]->setVisible(true);
						isEditingBoardVisible = false;
					}
				}
				else if (isEditingHole)
				{
					if (!board->getHolesMatrix()[i][j])
					{
						createHoleOnBoard(i, j);
					}
					else
					{
						board->getHolesMatrix()[i][j]->removeFromParentAndCleanup(true);
						board->getHolesMatrix()[i][j] = nullptr;
					}
				}
				else if (isEditingHint)
				{
					if (board->getEmptyMatrix()[i][j]->getColor() == Color3B::WHITE)
					{
						colorEmptyMatrix(i, j);
					}
					else
					{
						board->getEmptyMatrix()[i][j]->setColor(Color3B::WHITE);
					}
				}
				return true;
			}
		}
	}
	return false;
}

void EditorScene::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
	int sz = Sprite::create(BOARD)->getContentSize().width*scalePipe;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j]->getPosition().distance(touch->getLocation()) < sz/2)
			{
				if (board->getEmptyMatrix()[i][j]->getColor() == Color3B::WHITE)
				{
					if (isEditingHint && (board->getEmptyMatrix()[i][j]->isVisible() == true))
					{
						colorEmptyMatrix(i, j);
					}
					else if (isEditingBoard)
					{
						if (isEditingBoardVisible)
						{
							board->getEmptyMatrix()[i][j]->setVisible(false);
						}
						else
						{
							board->getEmptyMatrix()[i][j]->setVisible(true);
						}
					}
				}
			}
		}
	}
}

void EditorScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void EditorScene::onTouchCancelled(Touch *touch, cocos2d::Event *event)
{
	onTouchEnded(touch, event);
}
