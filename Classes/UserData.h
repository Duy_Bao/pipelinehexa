#ifndef __USER_DATA_H__
#define __USER_DATA_H__

#include "cocos2d.h"
#include "Definition.h"
#include <ctime>
#include "GameManager.h"
using namespace cocos2d;

class UserData
{
private:
	static UserData* m_instance;
	UserData();
public:
	static UserData* getInstance();
	~UserData();
	void saveLevelIntoFile();
	void loadLevelFromFile();
	//void saveSateIntoFile();
	//void loadStateFromFile();
	void saveDataIntoFile();
	void loadDataFromFile();
	void reset();
	bool getIsPlaying();
	void setIsPlaying(bool newValue);
	std::string choosePathPackage(int mode);
	void setNameLevelFileToLoad(std::string name);
	std::string getNameLevelFileToLoad();

	void setNameLevelFileToSave(std::string name);
	std::string getNameLevelFileToSave();

	
protected:
	
	std::string getURLSaveData();

	void setDataFile(DataSave dataSave);
	DataSave getDataFile();

	void setSaveFile(LevelState stateSave);
	LevelState getSaveFile();
	
	
public:
	LevelState m_levelSave;
	DataSave m_dataSave;
	bool m_isPlaying;
	std::string m_levelFileToSave;
	std::string m_levelFileToLoad;
};

#endif // __USER_DATA_H__
