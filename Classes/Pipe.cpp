#include "Pipe.h"
#include "Definition.h"
#include "SimpleAudioEngine.h"
#include "GameManager.h"
USING_NS_CC;


Pipe::Pipe(TYPEPIPE newType,COLOR newColor, int newCol, int newRow)
{
	m_type = newType;
	m_col = newCol;
	m_color = newColor;
	m_row = newRow;
	m_checked = false;
	m_dir = NONEDIR;
	scalePipe = GameManager::getInstance()->getScalePipe();
	auto filename = chooseType(newType);
	m_pipeSprite = Sprite::create(filename);
	m_pipeSprite->setScale(scalePipe);
	m_pipeSprite->setColor(chooseColor(newColor));
	this->setAnchorPoint(Vec2(0.5, 0.5));
	this->setContentSize(m_pipeSprite->getContentSize());
	m_pipeSprite->setPosition(m_pipeSprite->getContentSize() / 2);
	this->addChild(m_pipeSprite);
}
Pipe::~Pipe()
{
	this->release();
	m_pipeSprite->removeFromParent();
}

std::string Pipe::chooseType(TYPEPIPE type)
{
	std::string  name;
	switch (type)
	{
	case TYPEPIPE::STRAIGHT:
	{
		name = STRAIGHT_PIPE;
		break;
	}
	case TYPEPIPE::HOLE:
	{
		name = HOLE_PIPE;
		break;
	}
	case TYPEPIPE::V_PIPE_60:
	{
		name = V60_PIPE;
		break;
	}
	case TYPEPIPE::V_PIPE_120:
	{
		name = V120_PIPE;
		break;
	}
	case TYPEPIPE::END:
	{
		name = END_PIPE;
		break;
	}
	default:
		break;
	}
	return name;
}

cocos2d::Color3B Pipe::chooseColor(COLOR incolor)
{
	Color3B color;
	switch (incolor)
	{
	case COLOR::RED:
	{
		color = Color3B::RED;
		break;
	}
	case COLOR::BLUE:
	{
		color = Color3B::BLUE;
		break;
	}
	case COLOR::YELLOW:
	{
		color = Color3B::YELLOW;
		break;
	}
	case COLOR::GREEN:
	{
		color = Color3B::GREEN;
		break;
	}
	case COLOR::PURPLE:
	{
		color = Color3B::MAGENTA;
		break;
	}
	default:
		break;
	}
	return color;
}
Sprite* Pipe::getPipeSprite()
{
	return m_pipeSprite;
}

TYPEPIPE Pipe::getTypePipe()
{
	return m_type;
}

void Pipe::setCol(int new_col)
{
	m_col = new_col;
}

void Pipe::setRow(int new_row)
{
	m_row = new_row;
}

Rect Pipe::getBounding()
{
	return m_pipeSprite->getBoundingBox();
}

Size Pipe::getSizePipe()
{
	return m_pipeSprite->getContentSize();
}

int Pipe::getRow()
{
	return m_row;
}

int Pipe::getCol()
{
	return m_col;
}
COLOR Pipe::getColor()
{
	return m_color;
}
void Pipe::setColor(COLOR newValue)
{
	m_color = newValue;
}

bool Pipe::getChecked()
{
	return m_checked;
}

void Pipe::setChecked(bool newValue)
{
	m_checked = newValue;
}
void Pipe::setDirection(DIR newValue)
{
	m_dir = newValue;
}
DIR Pipe::getDirection()
{
	return m_dir;
}