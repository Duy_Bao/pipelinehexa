#include "SimpleAudioEngine.h"
#include "ScenePlay.h"
#include "HomeScene.h"
#include "LevelsScene.h"
#include "UserData.h"
#include "../ui/CocosGUI.h"
using namespace cocos2d::ui;
USING_NS_CC;


Scene* LevelsScene::createScene()
{
	auto scene = Scene::create();
	auto layer = LevelsScene::create();
	scene->addChild(layer);
	return scene;
}
bool LevelsScene::init()
{
	if (!LayerColor::initWithColor(Color4B(31, 44, 72, 255)))
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	btn_back = MenuItemImage::create(BTN_BACK, BTN_BACK, CC_CALLBACK_0(LevelsScene::backToModeScene, this));
	btn_back->setPosition(WIN_SIZE.width/7, WIN_SIZE.height/1.07);
	btn_back->setScale(0.5f);
	auto sz = Director::getInstance()->getWinSize();
	Size contentSize = Size(sz.width, sz.height - 100);
	auto _pageView = PageView::create();
	_pageView->setContentSize(contentSize);
	_pageView->setDirection(PageView::Direction::HORIZONTAL);
	_pageView->setPosition(Vec2(0, sz.height / 2 - contentSize.height / 2));
	_pageView->setIndicatorEnabled(true);
	_pageView->setIndicatorIndexNodesColor(Color3B::WHITE);
	_pageView->setIndicatorPosition(Vec2(sz.width / 2, sz.height/15));
	_pageView->setIndicatorIndexNodesScale(0.3f);
	_pageView->setIndicatorSpaceBetweenIndexNodes(0);
	_pageView->setIndicatorSelectedIndexColor(Color3B::WHITE);
	this->addChild(_pageView);
	menu = Menu::create(btn_back, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);

	for (int i = 0; i < LEVEL_PAGE; i++)
	{
		auto layout = Layout::create();
		layout->setPosition(Vec2(0, 0));

		Node* nodeCenter = Node::create();
		nodeCenter->setPosition(contentSize / 2);
		layout->addChild(nodeCenter);


		_pageView->addPage(layout);
#define LEVEL_PADDING_X 100
#define LEVEL_PADDING_Y 100
		
		float centerX = (LEVEL_COLUMN -1.0f) / 2.0f;
		float centerY = (LEVEL_ROW -1.0f) / 2.0f;
		for (int c = 0; c < LEVEL_COLUMN; c++)
		{
			for (int r = 0; r < LEVEL_ROW; r++)
			{
				float x = ((float)c - centerX) * LEVEL_PADDING_X;
				float y = (centerY - (float)r) * LEVEL_PADDING_Y;
				Node* ndoChild = Node::create();
				nodeCenter->addChild(ndoChild);
				ndoChild->setPosition(x, y);
				auto btn = Button::create(BTN_LEVEL, BTN_LEVEL, BTN_LEVEL_DISABLED, Widget::TextureResType::LOCAL);
				btn->addClickEventListener(CC_CALLBACK_1(LevelsScene::onBtnLevelClicked, this));
				btn->setScale(0.5f);
				ndoChild->addChild(btn);
				int levelID = i * (LEVEL_COLUMN * LEVEL_ROW) + r * (LEVEL_COLUMN)+c + 1;
				auto lbLevel = Label::create(StringUtils::format("%d", levelID).c_str(), "fonts/impact.ttf", 40);
				if (levelID > GameManager::getInstance()->getDataSave().currentLevel)
				{
					btn->setEnabled(false);
					auto locked = Sprite::create(LOCKED);
					locked->setScale(0.5f);
					locked->setPosition(ndoChild->getContentSize().width,ndoChild->getContentSize().height/2);
					lbLevel->setVisible(false);
					ndoChild->addChild(locked);
				}
				else if (levelID <= GameManager::getInstance()->getDataSave().passedLevel)
				{
					auto numOfStar = (GameManager::getInstance()->getDataSave().starEachLevel[levelID] < 0) ? 0 : GameManager::getInstance()->getDataSave().starEachLevel[levelID];
					auto firstStar = Sprite::create(STAR);
					firstStar->setScale(0.5f);
					firstStar->setPosition(lbLevel->getPosition().x - 30, lbLevel->getPosition().y - 30);
					firstStar->setRotation(240);
					ndoChild->addChild(firstStar, 2);
					auto secondStar = Sprite::create(STAR);
					secondStar->setScale(0.5f);
					secondStar->setPosition(lbLevel->getPosition().x, lbLevel->getPosition().y - 40);
					ndoChild->addChild(secondStar, 2);
					auto thirdStar = Sprite::create(STAR);
					thirdStar->setScale(0.5f);
					thirdStar->setPosition(lbLevel->getPosition().x + 30, lbLevel->getPosition().y - 30);
					thirdStar->setRotation(120);
					ndoChild->addChild(thirdStar, 2);
					if (numOfStar == 1)
					{
						firstStar->setColor(Color3B::YELLOW);
					}
					else if (numOfStar == 2)
					{
						firstStar->setColor(Color3B::YELLOW);
						secondStar->setColor(Color3B::YELLOW);
					}
					else if (numOfStar == 3)
					{
						firstStar->setColor(Color3B::YELLOW);
						secondStar->setColor(Color3B::YELLOW);
						thirdStar->setColor(Color3B::YELLOW);
					}
				}
				lbLevel->setTextColor(Color4B::WHITE);
				ndoChild->addChild(lbLevel, 1);
				btn->setTag(levelID);
			}
		}
	}
	this->scheduleUpdate();
	return true;
}
void LevelsScene::backToModeScene()
{
	auto scene = HomeScene::createScene();
	Director::getInstance()->replaceScene(TransitionProgressOutIn::create(0.5f, scene));
}
void LevelsScene::goToScenePlay(int level)
{
	GameManager::getInstance()->setCurrentLevel(level);
	UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%i.sav", level));
	UserData::getInstance()->loadLevelFromFile();
	GameManager::getInstance()->setWidth(GameManager::getInstance()->getLevelState().width);
	GameManager::getInstance()->setHeight(GameManager::getInstance()->getLevelState().height);
	GameManager::getInstance()->setScaleBoard(GameManager::getInstance()->getLevelState().scaleBoard);
	GameManager::getInstance()->setScalePipe(GameManager::getInstance()->getLevelState().scalePipe);
	auto scene = ScenePlay::createScene();
	Director::getInstance()->replaceScene(TransitionProgressInOut::create(0.5f, scene));
}
void LevelsScene::onBtnLevelClicked(Ref* ndo)
{
	int levelid = ((Node*)ndo)->getTag();
	goToScenePlay(levelid);
}