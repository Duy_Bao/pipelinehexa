#include "LevelSuccessLayer.h"
#include "SimpleAudioEngine.h"
#include "ScenePlay.h"
#include "UserData.h"
#include "Board.h"
USING_NS_CC;


bool LevelSuccessLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto levelSuccessBoard = Sprite::create(SUCCESS_LAYER);
	levelSuccessBoard->setScale(1.5f);
	this->addChild(levelSuccessBoard);
	btn_LevelSuccess_Next = MenuItemImage::create(BTN_NEXT_LEVEL, BTN_NEXT_LEVEL, CC_CALLBACK_1(LevelSuccessLayer::nextLevel, this));
	btn_LevelSuccess_Next->setPosition(levelSuccessBoard->getPosition().x + WIN_SIZE.width/6, levelSuccessBoard->getPosition().y - WIN_SIZE.height /7.7);
	btn_LevelSuccess_Next->setColor(Color3B::GREEN);
	btn_LevelSuccess_Next->setVisible(false);

	btn_LevelSuccess_Restart = MenuItemImage::create(BTN_RESTART, BTN_RESTART, CC_CALLBACK_1(LevelSuccessLayer::restartGame, this));
	btn_LevelSuccess_Restart->setPosition(levelSuccessBoard->getPosition().x - WIN_SIZE.width / 6, levelSuccessBoard->getPosition().y - WIN_SIZE.height / 7.7);
	btn_LevelSuccess_Restart->setColor(Color3B::ORANGE);
	btn_LevelSuccess_Restart->setVisible(false);

	levelSuccessMenu = Menu::create(btn_LevelSuccess_Restart, btn_LevelSuccess_Next, NULL);
	levelSuccessMenu->setPosition(Vec2::ZERO);
	this->addChild(levelSuccessMenu);

	firstStar = Sprite::create(STAR);
	firstStar->setPosition(levelSuccessBoard->getPosition().x - WIN_SIZE.width / 6, levelSuccessBoard->getPosition().y + WIN_SIZE.height / 28);
	firstStar->setRotation(120);
	firstStar->setScale(0);
	this->addChild(firstStar);
	secondStar = Sprite::create(STAR);
	secondStar->setPosition(levelSuccessBoard->getPosition().x, firstStar->getPosition().y + WIN_SIZE.height / 30);
	secondStar->setScale(0);
	this->addChild(secondStar);
	thirdStar = Sprite::create(STAR);
	thirdStar->setPosition(levelSuccessBoard->getPosition().x + WIN_SIZE.width/6, levelSuccessBoard->getPosition().y + WIN_SIZE.height / 28);
	thirdStar->setRotation(240);
	thirdStar->setScale(0);
	this->addChild(thirdStar);

	labelComplete = Label::createWithTTF("Complete in 0 move", "fonts/impact.ttf", 40);
	labelComplete->setPosition(levelSuccessBoard->getPosition().x, levelSuccessBoard->getPosition().y - WIN_SIZE.height/ 17);
	labelComplete->setScale(0);
	this->addChild(labelComplete);

	this->scheduleUpdate();
	isShow = false;
    return true;
}
void LevelSuccessLayer::restartGame(cocos2d::Ref *sender)
{
	this->hide();
	auto scene = ScenePlay::createScene();
	Director::getInstance()->replaceScene(TransitionProgressRadialCCW::create(0.5f, scene));
}
void LevelSuccessLayer::nextLevel(cocos2d::Ref *sender)
{
	this->hide();
	UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%i.sav", GameManager::getInstance()->getCurrentLevel() + 1));
	UserData::getInstance()->loadLevelFromFile();
	GameManager::getInstance()->setCurrentLevel(GameManager::getInstance()->getCurrentLevel() + 1);
	auto scene = ScenePlay::createScene();
	Director::getInstance()->replaceScene(TransitionMoveInR::create(0.5f, scene));
}
void LevelSuccessLayer::actionAppear(Vec2 posEnd)
{
	auto action = MoveTo::create(0.7f, posEnd);
	this->stopAllActions();
	this->runAction(EaseBackInOut::create(action));
}
void LevelSuccessLayer::swallowTouch()
{
	m_pTouchListener = EventListenerTouchOneByOne::create();
	m_pTouchListener->setSwallowTouches(true);
	m_pTouchListener->onTouchBegan = CC_CALLBACK_2(LevelSuccessLayer::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(m_pTouchListener, this);
}

bool LevelSuccessLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	return true;
}
void LevelSuccessLayer::showStarAndButton(int numOfStar)
{
	labelComplete->setString(StringUtils::format("Complete in %i move", GameManager::getInstance()->getNumOfMove()));
	labelComplete->runAction(ScaleTo::create(0.2f, 1.0f));
	firstStar->setVisible(true);
	secondStar->setVisible(true);
	thirdStar->setVisible(true); 
	if (numOfStar == 1)
	{
		firstStar->setColor(Color3B::YELLOW);
	}
	else if (numOfStar == 2)
	{
		firstStar->setColor(Color3B::YELLOW);
		secondStar->setColor(Color3B::YELLOW);
	}
	else
	{
		firstStar->setColor(Color3B::YELLOW);
		secondStar->setColor(Color3B::YELLOW);
		thirdStar->setColor(Color3B::YELLOW);
	}
	firstStar->runAction(Sequence::create(ScaleTo::create(0.2f, 1.6f), ScaleTo::create(0.2f, 1.2f), nullptr));
	secondStar->runAction(Sequence::create(ScaleTo::create(0.2f, 1.9f), ScaleTo::create(0.2f, 1.5f), nullptr));
	thirdStar->runAction(Sequence::create(ScaleTo::create(0.2f, 1.6f), ScaleTo::create(0.2f, 1.2f), nullptr));
	btn_LevelSuccess_Next->setVisible(true);
	btn_LevelSuccess_Restart->setVisible(true);
}
void LevelSuccessLayer::show()
{
	swallowTouch();
	isShow = true;
	Vec2 posShow = Vec2(WIN_SIZE.width / 2, WIN_SIZE.height / 2);
	auto action = MoveTo::create(0.7f, posShow);
	this->stopAllActions();
	this->runAction(Sequence::create(EaseBackInOut::create(action), DelayTime::create(0.1f), CallFunc::create([&]()
	{
		showStarAndButton(GameManager::getInstance()->getStarEarn());
	}),nullptr));
	
}
void LevelSuccessLayer::hide()
{
	Vec2 posHide = Vec2(WIN_SIZE.width / 2, -WIN_SIZE.height / 2);
	isShow = false;
	Director::getInstance()->getEventDispatcher()->removeEventListener(m_pTouchListener);
	auto action = MoveTo::create(0.7f, posHide);
	this->stopAllActions();
	this->runAction(EaseBackInOut::create(action));
}

