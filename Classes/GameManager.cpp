#include "GameManager.h"
#include "Definition.h"
USING_NS_CC;

GameManager* GameManager::m_instance = NULL;
GameManager::GameManager()
{
	beginFromHole = false;
	endDrag = false;
	isLoadLevel = false;
	nextDirection = NONEDIR;
	lastDirection = NONEDIR;
	typePipeCurve = STRAIGHT;
	checkedHole = false;
	pipeCurveRotation = 0;
	nextpipeCurveRotation = 0;
	currentLevel = 1;
	currentMode = 1;
	starEarnedInLevel = 0;
	numOfMove = 0;
	widthBoard = 5;
	heightBoard = 6;
	scaleBoard = 0.45f;
	scalePipe = 0.45f;
}
GameManager::~GameManager()
{

}
GameManager* GameManager::getInstance()
{
	if (m_instance == NULL)
	{
		m_instance = new GameManager();
	}

	return m_instance;
}

COLOR GameManager::getColorCurrent()
{
	return colorCurrent;
}

void GameManager::setColorCurrent(COLOR newValue)
{
	colorCurrent = newValue;
}
bool GameManager::getBeginFromHole()
{
	return beginFromHole;
}
void GameManager::setBeginFromHole(bool newValue)
{
	beginFromHole = newValue;
}

bool GameManager::getEndDrag()
{
	return endDrag;
}

void GameManager::setEndDrag(bool newValue)
{
	endDrag = newValue;
}


bool GameManager::getCheckedHole()
{
	return checkedHole;
}

void GameManager::setCheckedHole(bool newValue)
{
	checkedHole = newValue;
}

DIR GameManager::getNextDirection()
{
	return nextDirection;
}

void GameManager::setNextDirection(DIR newValue)
{
	nextDirection = newValue;
}

DIR GameManager::getLastDirection()
{
	return lastDirection;
}

void GameManager::setLastDirection(DIR newValue)
{
	lastDirection = newValue;
}

TYPEPIPE GameManager::getTypePipeCurve()
{
	return typePipeCurve;
}

void GameManager::setTypePipeCurve(TYPEPIPE newValue)
{
	typePipeCurve = newValue;
}

int GameManager::getPipeCurveRotation()
{
	return pipeCurveRotation;
}

void GameManager::setPipeCurveRotation(int newValue)
{
	pipeCurveRotation = newValue;
}
int GameManager::getNextPipeCurveRotation()
{
	return nextpipeCurveRotation;
}

void GameManager::setNextPipeCurveRotation(int newValue)
{
	nextpipeCurveRotation = newValue;
}

LevelState GameManager::getLevelState()
{
	return m_levelState;
}

void GameManager::setLevelState(LevelState newLevel)
{
	m_levelState = newLevel;
}

DataSave GameManager::getDataSave()
{
	return m_dataSave;
}

void GameManager::setDataSave(DataSave newData)
{
	m_dataSave = newData;
}

int GameManager::getCurrentLevel()
{
	return currentLevel;
}
void GameManager::setCurrentLevel(int newValue)
{
	currentLevel = newValue;
}


int GameManager::getCurrentMode()
{
	return currentMode;
}


void GameManager::setCurrentMode(int newValue)
{
	currentMode == newValue;
}

int GameManager::getCurrentPackage()
{
	return currentPackage;
}

void GameManager::setCurrentPackage(int newValue)
{
	currentPackage == newValue;
}

int GameManager::getStarEarn()
{
	return starEarnedInLevel;
}

void GameManager::setStarEarn(int newValue)
{
	starEarnedInLevel = newValue;
}
int GameManager::getNumOfMove()
{
	return numOfMove;
}

void GameManager::setNumOfMove(int newValue)
{
	numOfMove = newValue;
}

bool GameManager::getLoadLevel()
{
	return isLoadLevel;
}

void GameManager::setLoadLevel(bool newValue)
{
	isLoadLevel = newValue;
}

int GameManager::getWidth()
{
	return widthBoard;
}

void GameManager::setWidth(int newValue)
{
	widthBoard = newValue;
}

int GameManager::getHeight()
{
	return heightBoard;
}

void GameManager::setHeight(int newValue)
{
	heightBoard = newValue;
}

float GameManager::getScaleBoard()
{
	return scaleBoard;
}

void GameManager::setScaleBoard(float newValue)
{
	scaleBoard = newValue;
}

void GameManager::setScalePipe(float newValue)
{
	scalePipe = newValue;
}

float GameManager::getScalePipe()
{
	return scalePipe;
}
