#ifndef __DEFINITION_H__
#define __DEFINITION_H__

#include "cocos2d.h"
using namespace cocos2d;
enum DIR {NONEDIR,TOP,DOWN,TOP_LEFT,BOT_LEFT,TOP_RIGHT,BOT_RIGHT};
enum COLOR {NONECOLOR,RED,YELLOW,GREEN, BLUE,PURPLE};
enum TYPEPIPE {NONETYPE,HOLE, END, STRAIGHT, V_PIPE_60, V_PIPE_120};
enum LOCALZ {BACKGROUND, BOARDMATRIX,HOLEMATRIX, PIPEMATRIX,UI,LAYER};
enum MODE {GAME,EDITOR};
#define WIN_SIZE Director::getInstance()->getWinSize()


#define BASE_WIDTH 5
#define BASE_HEIGHT 6
#define SCALE_BOARD 0.45f
#define SCALE_PIPE 0.45f

#define MAX_COLOR 4

//UI
#define BTN "btn.png"
#define BTN_HOME_PLAY "right.png"
#define BTN_NEXT_LEVEL "fastForward.png"
#define BTN_PREVIOUS_LEVEL "rewind.png"
#define BTN_LEVEL "btn_Level.png"
#define BTN_LEVEL_DISABLED "btn_Level_Disabled.png"
#define BTN_HINT "zoom.png"
#define BTN_RESTART "return.png"
#define BTN_BACK "left.png"
#define BTN_MODE "btn_mode.png"
#define STAR "star.png"
#define LOCKED "locked.png"

//Matrix
#define BOARD "cell_board.png"
#define STRAIGHT_PIPE "straight_pipe.png"
#define V60_PIPE "vpipe60.png"
#define V120_PIPE "vpipe120.png"
#define HOLE_PIPE "hole.png"
#define END_PIPE "end_pipe.png"

//Layer
#define SUCCESS_LAYER "levelCleared.png"

//Pos 

#define OFFSET_X 0.882f
#define OFFSET_Y 0.764f
// State save and level
#define MODE_SAVE_SIZE 300

#define PATH_LEVEL "levels/"
#define PATH_NOVICE_LEVELS "packages/novice/"
#define PATH_BEGINER_LEVELS "packages/beginner/"
#define PATH_ADVANCE_LEVELS "packages/advance/"
#define PATH_EXPERT_LEVELS "packages/expert/"
#define PATH_MASTER_LEVELS "packages/master/"

#define LEVEL_PAGE 4
#define LEVEL_ROW 6
#define LEVEL_COLUMN 5

#define MODE_PAGE 1
#define MODE_ROW 5
#define MODE_COLUMN 1

struct DataSave
{
	int currentLevel = 1;
	int passedLevel = 0;
	int currentMode = 1;
	int passedMode = 0;
	int allStarEarned = 0;
	int starEachLevel[LEVEL_COLUMN*LEVEL_ROW*LEVEL_PAGE + 1];
	int starEachMode[MODE_COLUMN*MODE_ROW*MODE_PAGE + 1];
};
struct LevelState
{
	int width;
	int height;
	float scaleBoard;
	float scalePipe;
	int board[20][20];
	int hole_Color[20][20];
	Vec2 redHint[50];
	Vec2 yellowHint[50];
	Vec2 blueHint[50];
	Vec2 greenHint[50];
	Vec2 purpleHint[50];
	int maxMove;
	int bestMove;
	int starEarned;
};

#endif // __DEFINITION_H__
