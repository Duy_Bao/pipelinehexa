#ifndef __MODE_SCENE_H__
#define __MODE_SCENE_H__

#include <iostream>
#include <ctime>
#include "cocos2d.h"
class ModeScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
	void backToHomeScene();
	void goToLevelScene(int mode);
	
	CREATE_FUNC(ModeScene);
private:
	void onBtnModeClicked(Ref* ndo);
	std::string chooseModeName(int id);
	MenuItemImage *btn_back;
	Menu *menu;
};

#endif // __LEVELS_SCENE_H__
