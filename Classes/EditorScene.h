#ifndef __EDITOR_SCENE_H__
#define __EDITOR_SCENE_H__

#include "cocos2d.h"
#include <iostream>
#include <ctime>
#include "Board.h"
#include "Pipe.h"
#include "UserData.h"
#include "../ui/CocosGUI.h"
class EditorScene : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void initBoard();
	void initButton();
	void touchEvent();
	void clearBoard(cocos2d::Ref *sender);
	void colorEmptyMatrix(int col, int row);
	void changeHoleColor(cocos2d::Ref *sender);
	void changeSizeBoard(cocos2d::Ref *sender);
	void createHoleOnBoard(int col, int row);
	void saveHintToList(int col, int row);
	void saveHint(LevelState &levelState);
	int getColorFromEmptyMatrix(int col, int row);
	void clearHole(Ref *ref);
	void editBoard(cocos2d::Ref *sender);
	void editHole(cocos2d::Ref *sender);
	void editHint(cocos2d::Ref *sender);
	void saveLevel(cocos2d::Ref *sender);
	void loadLevel(cocos2d::Ref *sender);
	void changePackage(cocos2d::Ref *sender);
	int checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchCancelled(Touch *touch, cocos2d::Event *event);
	CREATE_FUNC(EditorScene);
private:
	Board* board;
	Menu *menu;
	MenuItemImage *btn_HoleColor;
	MenuItemImage *btn_SaveLevel;
	MenuItemImage *btn_ClearBoard;
	MenuItemImage *btn_ClearHole;
	MenuItemImage *btn_EditBoard;
	MenuItemImage *btn_EditHole;
	MenuItemImage *btn_SizeBoard;
	MenuItemImage *btn_LoadLevel;
	MenuItemImage *btn_EditHint;
	MenuItemImage *btn_Package;
	Label *holeColorLabel;
	Label *changePackageLabel;
	Label *clearBoardLabel;
	Label *clearHoleLabel;
	Label *saveLabel;
	Label *sizeBoardLabel;
	Label *editBoardLabel;
	Label *editHoleLabel;
	Label *editHintLabel;
	Label *loadLevelLabel;
	cocos2d::ui::TextField *txt;
	bool isEditingBoard;
	bool isEditingBoardVisible;
	bool isEditingHole;
	bool isEditingHint;
	float scalePipe;
	float scaleBoard;
	int widthBoard;
	int heightBoard;
	COLOR currentEditColor;
	int currentEditPackage;
	bool loaded;
	std::vector<Vec2> listRedHint;
	std::vector<Vec2> listBlueHint;
	std::vector<Vec2> listYellowHint;
	std::vector<Vec2> listGreenHint;
	std::vector<Vec2> listPurpleHint;
};
#endif // __HELLOWORLD_SCENE_H__
