#ifndef __PIPE_H__
#define __PIPE_H__

#include "cocos2d.h"
#include <ctime>
#include "Definition.h"
using namespace cocos2d;

class Pipe : public cocos2d::Node
{
public:
	Pipe(TYPEPIPE newType,COLOR newColor, int newCol, int newRow);
	~Pipe();
	std::string chooseType(TYPEPIPE type);
	Color3B chooseColor(COLOR color);
	Sprite* getPipeSprite();
	TYPEPIPE getTypePipe();
	void setCol(int new_col);
	void setRow(int new_row);
	Rect getBounding();
	Size getSizePipe();
	int getRow();
	int getCol();
	bool getChecked();
	void setChecked(bool newValue);
	void setDirection(DIR newValue);
	DIR getDirection();
	COLOR getColor();
	void setColor(COLOR newValue);
private:
	Sprite *m_pipeSprite;
	TYPEPIPE m_type;
	COLOR m_color;
	int m_row;
	int m_col;
	DIR m_dir;
	bool m_checked;
	float scalePipe;
};

#endif // __PIPE_H__
