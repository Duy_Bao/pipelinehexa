﻿#include "SimpleAudioEngine.h"
#include "ScenePlay.h"
#include "HomeScene.h"
#include "ModeScene.h"
#include "UserData.h"
#include "LevelsScene.h"
#include "../ui/CocosGUI.h"
using namespace cocos2d::ui;
USING_NS_CC;


Scene* ModeScene::createScene()
{
	auto scene = Scene::create();
	auto layer = ModeScene::create();
	scene->addChild(layer);
	return scene;
}
bool ModeScene::init()
{
	if (!LayerColor::initWithColor(Color4B(31, 44, 72, 255)))
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	btn_back = MenuItemImage::create(BTN_BACK, BTN_BACK, CC_CALLBACK_0(ModeScene::backToHomeScene, this));
	btn_back->setPosition(WIN_SIZE.width/7, WIN_SIZE.height/1.07);
	btn_back->setScale(0.5f);
	auto sz = Director::getInstance()->getWinSize();
	Size contentSize = Size(sz.width, sz.height - 100);
	auto _pageView = PageView::create();
	_pageView->setContentSize(contentSize);
	_pageView->setDirection(PageView::Direction::HORIZONTAL);
	_pageView->setPosition(Vec2(0, sz.height / 2 - contentSize.height / 2));
	_pageView->setIndicatorEnabled(true);
	_pageView->setIndicatorIndexNodesColor(Color3B::WHITE);
	_pageView->setIndicatorPosition(Vec2(sz.width / 2, sz.height/15));
	_pageView->setIndicatorIndexNodesScale(0.3f);
	_pageView->setIndicatorSpaceBetweenIndexNodes(0);
	_pageView->setIndicatorSelectedIndexColor(Color3B::WHITE);
	this->addChild(_pageView);
	menu = Menu::create(btn_back, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);

	for (int i = 0; i < MODE_PAGE; i++)
	{
		auto layout = Layout::create();
		layout->setPosition(Vec2(0, 0));

		Node* nodeCenter = Node::create();
		nodeCenter->setPosition(contentSize / 2);
		layout->addChild(nodeCenter);


		_pageView->addPage(layout);
#define MODE_PADDING_X 100
#define MODE_PADDING_Y 150
		
		float centerX = (MODE_COLUMN -1.0f) / 2.0f;
		float centerY = (MODE_ROW -1.0f) / 2.0f;
		for (int c = 0; c < MODE_COLUMN; c++)
		{
			for (int r = 0; r < MODE_ROW; r++)
			{
				float x = ((float)c - centerX) * MODE_PADDING_X;
				float y = (centerY - (float)r) * MODE_PADDING_Y;
				Node* ndoChild = Node::create();
				nodeCenter->addChild(ndoChild);
				ndoChild->setPosition(x, y);
				auto btn = Button::create(BTN_MODE, BTN_MODE, BTN_MODE, Widget::TextureResType::LOCAL);
				btn->setScaleX(1.5f);
				btn->setColor(Color3B::ORANGE);
				btn->addClickEventListener(CC_CALLBACK_1(ModeScene::onBtnModeClicked, this));
				ndoChild->addChild(btn);
				int modeID = i * (MODE_COLUMN * MODE_ROW) + r * (MODE_COLUMN) + c + 1;
				auto modeName = chooseModeName(modeID);
				auto lbMode = Label::create(modeName, "fonts/impact.ttf", 40);
				lbMode->setTextColor(Color4B::WHITE);
				ndoChild->addChild(lbMode, 1);
				auto numOfStarMode = (GameManager::getInstance()->getDataSave().starEachMode[modeID] < 0) ? 0 : GameManager::getInstance()->getDataSave().starEachMode[modeID];
				auto lbStar = Label::create(StringUtils::format("%i/%i", numOfStarMode,3 * LEVEL_COLUMN * LEVEL_ROW * LEVEL_PAGE).c_str(), "fonts/impact.ttf", 40);
				lbStar->setPosition(lbMode->getPosition().x + 150, lbMode->getPosition().y);
				ndoChild->addChild(lbStar);
				if (modeID > GameManager::getInstance()->getDataSave().currentMode)
				{
					btn->setEnabled(false);
					auto locked = Sprite::create(LOCKED);
					locked->setScale(0.5f);
					locked->setPosition(ndoChild->getContentSize().width,ndoChild->getContentSize().height/2);
					lbMode->setVisible(false);
					lbStar->setVisible(false);
					ndoChild->addChild(locked);
				}
				
				btn->setTag(modeID);
			}
		}
	}
	this->scheduleUpdate();
	return true;
}
void ModeScene::backToHomeScene()
{
	auto scene = HomeScene::createScene();
	Director::getInstance()->replaceScene(TransitionProgressOutIn::create(0.5f, scene));
}
void ModeScene::goToLevelScene(int mode)
{
	//GameManager::getInstance()->setCurrentLevel(mode);
	//UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%i.sav", mode));
	//UserData::getInstance()->loadLevelFromFile();
	auto scene = LevelsScene::createScene();
	Director::getInstance()->replaceScene(TransitionProgressInOut::create(0.5f, scene));
}
void ModeScene::onBtnModeClicked(Ref* ndo)
{
	int modeID = ((Node*)ndo)->getTag();
	goToLevelScene(modeID);
}
std::string ModeScene::chooseModeName(int id)
{
	switch (id)
	{
		case 1:
		{
			return "NOVICE";
			break;
		}
		case 2:
		{
			return "BEGINNER";
			break;
		}
		case 3:
		{
			return "ADVANCE";
			break;
		}
		case 4:
		{
			return "EXPERT";
			break;
		}
		case 5:
		{
			return "MASTER";
			break;
		}
	}
}