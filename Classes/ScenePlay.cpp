#include "ScenePlay.h"
#include "LevelsScene.h"
#include "SimpleAudioEngine.h"
#include "GameManager.h"
USING_NS_CC;


cocos2d::Scene* ScenePlay::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = ScenePlay::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}
// on "init" you need to initialize your instance
bool ScenePlay::init()
{
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(192, 192, 192, 255)))
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	initDefine();
	initBoard();
	initLayer();
	initButton();
	initLabel();
	initStar();
	touchEvent();

	this->scheduleUpdate();
	return true;
}

void ScenePlay::initStar()
{
	firstStar = Sprite::create(STAR);
	firstStar->setPosition(bestMoveLabel->getPosition().x - 100, WIN_SIZE.height / 1.03);
	this->addChild(firstStar, UI);
	secondStar = Sprite::create(STAR);
	secondStar->setPosition(firstStar->getPosition().x + 70, WIN_SIZE.height / 1.03);
	this->addChild(secondStar, UI);
	thirdStar = Sprite::create(STAR);
	thirdStar->setPosition(firstStar->getPosition().x + 140, WIN_SIZE.height / 1.03);
	this->addChild(thirdStar, UI);
	if (numOfStarEarned == 1)
	{
		firstStar->setColor(Color3B::YELLOW);
	}
	else if (numOfStarEarned == 2)
	{
		firstStar->setColor(Color3B::YELLOW);
		secondStar->setColor(Color3B::YELLOW);
	}
	else if ( numOfStarEarned == 3)
	{
		firstStar->setColor(Color3B::YELLOW);
		secondStar->setColor(Color3B::YELLOW);
		thirdStar->setColor(Color3B::YELLOW);
	}
}
void ScenePlay::initDefine()
{
	showRedHint = false;
	showYellowHint = false;
	showBlueHint = false;
	showGreenHint = false;
	showPurpleHint = false;
	numOfMove = 0;
	widthBoard = GameManager::getInstance()->getWidth();
	heightBoard = GameManager::getInstance()->getHeight();
	scaleBoard = GameManager::getInstance()->getScaleBoard();
	scalePipe = GameManager::getInstance()->getScalePipe();
	numOfMaxMove = GameManager::getInstance()->getLevelState().maxMove;
	numOfStarEarned = GameManager::getInstance()->getLevelState().starEarned;
	bestNumOfMove = GameManager::getInstance()->getLevelState().bestMove;
}
void ScenePlay::initBoard()
{
	board = new Board(this, false);
}
void ScenePlay::initButton()
{
	btn_restart = MenuItemImage::create(BTN_RESTART, BTN_RESTART, CC_CALLBACK_1(ScenePlay::buttonRestart, this));
	btn_restart->setScale(0.7f);
	btn_restart->setColor(Color3B::BLACK);
	btn_restart->setPosition(WIN_SIZE.width / 2, WIN_SIZE.height / 9);

	btn_back = MenuItemImage::create(BTN_BACK,BTN_BACK, CC_CALLBACK_1(ScenePlay::buttonBack, this));
	btn_back->setScale(0.7f);
	btn_back->setColor(Color3B::BLACK);
	btn_back->setPosition(btn_restart->getPositionX() - 200, WIN_SIZE.height / 9);

	btn_preLevel = MenuItemImage::create(BTN_PREVIOUS_LEVEL, BTN_PREVIOUS_LEVEL, CC_CALLBACK_1(ScenePlay::buttonPreLevel, this));
	btn_preLevel->setScale(0.7f);
	btn_preLevel->setColor(Color3B::BLACK);
	btn_preLevel->setPosition(btn_restart->getPositionX() - 100, btn_back->getPositionY());

	btn_nextLevel = MenuItemImage::create(BTN_NEXT_LEVEL, BTN_NEXT_LEVEL, CC_CALLBACK_1(ScenePlay::buttonNextLevel, this));
	btn_nextLevel->setScale(0.7f);
	btn_nextLevel->setColor(Color3B::BLACK);
	btn_nextLevel->setPosition(btn_restart->getPositionX() + 100, WIN_SIZE.height / 9);

	btn_hint = MenuItemImage::create(BTN_HINT, BTN_HINT, CC_CALLBACK_1(ScenePlay::buttonHint, this));
	btn_hint->setScale(0.7f);
	btn_hint->setColor(Color3B::BLACK);
	btn_hint->setPosition(btn_restart->getPositionX() + 200, WIN_SIZE.height / 9);

	menu = Menu::create(btn_back , btn_preLevel , btn_restart , btn_nextLevel,btn_hint, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, UI);
}
void ScenePlay::initLayer()
{
	successLayer = LevelSuccessLayer::create();
	successLayer->setPosition(WIN_SIZE.width / 2, -WIN_SIZE.height / 2);
	this->addChild(successLayer, LAYER);
}
void ScenePlay::initLabel()
{
	fillPercentLabel = Label::createWithTTF("Fill: 0%", "fonts/impact.ttf", 40);
	fillPercentLabel->setPosition(WIN_SIZE.width / 2, WIN_SIZE.height / 1.10); 
	this->addChild(fillPercentLabel, UI);
	moveLabel = Label::createWithTTF(StringUtils::format("Move: 0/%i", numOfMaxMove), "fonts/impact.ttf", 40);
	moveLabel->setPosition(fillPercentLabel->getPositionX() - 200, WIN_SIZE.height / 1.10);
	this->addChild(moveLabel, UI);
	bestMoveLabel = Label::createWithTTF(StringUtils::format("Best: %i", bestNumOfMove), "fonts/impact.ttf", 40);
	bestMoveLabel->setPosition(fillPercentLabel->getPositionX() + 200, WIN_SIZE.height / 1.10);
	this->addChild(bestMoveLabel, UI);
	currentLevelLabel = Label::createWithTTF(StringUtils::format("LEVELS: %i", GameManager::getInstance()->getCurrentLevel()), "fonts/impact.ttf", 40);
	currentLevelLabel->setPosition(moveLabel->getPosition().x, moveLabel->getPosition().y + 70);
	this->addChild(currentLevelLabel);
}
void ScenePlay::touchEvent()
{
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(ScenePlay::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(ScenePlay::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(ScenePlay::onTouchEnded, this);
	listener->onTouchCancelled = CC_CALLBACK_2(ScenePlay::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	this->setTouchEnabled(true);
}
void ScenePlay::update(float dt)
{

}
void ScenePlay::buttonBack(Ref *node)
{
	auto scene = LevelsScene::createScene();
	Director::getInstance()->replaceScene(TransitionProgressOutIn::create(0.7f, scene));
}
void ScenePlay::buttonPreLevel(Ref *node)
{
	if (GameManager::getInstance()->getCurrentLevel() > 0 && GameManager::getInstance()->getCurrentLevel() != 1)
	{
		UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%i.sav", GameManager::getInstance()->getCurrentLevel() - 1));
		UserData::getInstance()->loadLevelFromFile();
		GameManager::getInstance()->setCurrentLevel(GameManager::getInstance()->getCurrentLevel() - 1);
		auto scene = ScenePlay::createScene();
		Director::getInstance()->replaceScene(TransitionMoveInL::create(0.5f, scene));
	}
}
void ScenePlay::buttonNextLevel(Ref *node)
{
	if (GameManager::getInstance()->getCurrentLevel() < GameManager::getInstance()->getDataSave().passedLevel)
	{
		UserData::getInstance()->setNameLevelFileToLoad(StringUtils::format("lv_%i.sav", GameManager::getInstance()->getCurrentLevel() + 1));
		UserData::getInstance()->loadLevelFromFile();
		GameManager::getInstance()->setCurrentLevel(GameManager::getInstance()->getCurrentLevel() + 1);
		auto scene = ScenePlay::createScene();
		Director::getInstance()->replaceScene(TransitionMoveInR::create(0.5f, scene));
	}
}
void ScenePlay::buttonHint(Ref *node)
{
	auto repeatTimes = 2;
	auto delay = DelayTime::create(0.5f);
	auto temp = GameManager::getInstance()->getLevelState();
	if (!showRedHint)
	{
		for (int i = 0; i < 50; i++)
		{
			if ((int)temp.redHint[i].x != -1 && (int)temp.redHint[i].y != -1)
			{
				auto tintRed = TintBy::create(0.75f, 0, 255, 255);
				auto tintRedBack = tintRed->reverse();
				auto seq = Sequence::create(tintRed,delay->clone(),tintRedBack, nullptr);
				board->getEmptyMatrix()[(int)temp.redHint[i].x][(int)temp.redHint[i].y]->runAction(Repeat::create(seq, repeatTimes));
			}
		}
		showRedHint = true;
		return;
	}
	else if (!showYellowHint)
	{
		for (int i = 0; i < 50; i++)
		{
			if ((int)temp.yellowHint[i].x != -1 && (int)temp.yellowHint[i].y != -1)
			{
				auto tintYellow = TintBy::create(0.75f, 0, 0, 255);
				auto tintYellowBack = tintYellow->reverse();
				auto seq = Sequence::create(tintYellow, delay->clone(), tintYellowBack, nullptr);
				board->getEmptyMatrix()[(int)temp.yellowHint[i].x][(int)temp.yellowHint[i].y]->runAction(Repeat::create(seq, repeatTimes));
			}
		}
		showYellowHint = true;
		return;
	}
	else if (!showGreenHint)
	{
		for (int i = 0; i < 50; i++)
		{
			if ((int)temp.greenHint[i].x != -1 && (int)temp.greenHint[i].y != -1)
			{
				auto tintGreen = TintBy::create(0.75f, 255, 0, 255);
				auto tintGreenBack = tintGreen->reverse();
				auto seq = Sequence::create(tintGreen, delay->clone(), tintGreenBack, nullptr);
				board->getEmptyMatrix()[(int)temp.greenHint[i].x][(int)temp.greenHint[i].y]->runAction(Repeat::create(seq, repeatTimes));
			}
		}
		showGreenHint = true;
		return;
	}
	else if (!showBlueHint)
	{
		for (int i = 0; i < 50; i++)
		{
			if ((int)temp.blueHint[i].x != -1 && (int)temp.blueHint[i].y != -1)
			{
				auto tintBlue = TintBy::create(0.75f, 255, 255, 0);
				auto tintBlueBack = tintBlue->reverse();
				auto seq = Sequence::create(tintBlue, delay->clone(), tintBlueBack, nullptr);
				board->getEmptyMatrix()[(int)temp.blueHint[i].x][(int)temp.blueHint[i].y]->runAction(Repeat::create(seq, repeatTimes));
			}
		}
		showBlueHint = true;
		return;
	}
	else if (!showPurpleHint)
	{
		for (int i = 0; i < 50; i++)
		{
			if ((int)temp.purpleHint[i].x != -1 && (int)temp.purpleHint[i].y != -1)
			{
				auto tintPurple = TintBy::create(0.75f, 0, 255, 0);
				auto tintPurpleBack = tintPurple->reverse();
				auto seq = Sequence::create(tintPurple, delay->clone(), tintPurpleBack, nullptr);
				board->getEmptyMatrix()[(int)temp.purpleHint[i].x][(int)temp.purpleHint[i].y]->runAction(Repeat::create(seq, repeatTimes));
			}
		}
		showPurpleHint = true;
		return;
	}
}
void ScenePlay::buttonRestart(Ref *node)
{
	auto scene = ScenePlay::createScene();
	Director::getInstance()->replaceScene(TransitionProgressRadialCCW::create(0.5f,scene));
}
int ScenePlay::checkVec2InList(Vec2 newValue, std::vector<cocos2d::Vec2> newList)
{
	for (int i = 0 ; i < newList.size();i++)
	{
		if ((int)newValue.x == (int)newList.at(i).x && (int)newValue.y == (int)newList.at(i).y )
		{
			return i;
		}
	}
	return -1;
}
void ScenePlay::handleTouch(int x, int y)
{
	DIR prePipeDirection;
	Vec2 index = Vec2(x, y);
	int id = checkVec2InList(index, listVec2);
	if (id == -1) 
	{
		listVec2.push_back(index);
		board->deleteAllPipeSameColor(GameManager::getInstance()->getColorCurrent());
		board->drawPipeOnList(listVec2);
	}
	else if (id != -1 && id != listVec2.size() - 1) 
	{
		popVector(listVec2, id);
		if (board->getPipesMatrix()[(int)index.x][(int)index.y])
		{
			if (board->getHolesMatrix()[(int)index.x][(int)index.y])
			{
				board->deleteAllPipeSameColor(GameManager::getInstance()->getColorCurrent());
			}
			else
			{
				prePipeDirection = board->getPipesMatrix()[(int)listVec2.at(id - 1).x][(int)listVec2.at(id - 1).y]->getDirection();
				int rotation = board->getRotationForGoBackWard(prePipeDirection);
				board->getEmptyMatrix()[(int)index.x][(int)index.y]->setColor(Color3B::WHITE);
				board->getPipesMatrix()[(int)index.x][(int)index.y]->removeFromParentAndCleanup(true);
				board->getPipesMatrix()[(int)index.x][(int)index.y] = nullptr;
				Pipe *pipe = new Pipe(END, GameManager::getInstance()->getColorCurrent(), (int)index.x, (int)index.y);
				pipe->setRotation(rotation);
				pipe->setPosition(board->getEmptyMatrix()[(int)index.x][(int)index.y]->getPosition());
				board->getPipesMatrix()[(int)index.x][(int)index.y] = pipe;
				this->addChild(board->getPipesMatrix()[(int)index.x][(int)index.y], PIPEMATRIX);
			}
		}
	}
}
void ScenePlay::popVector(std::vector<Vec2> &list, int id)
{
	int sz = list.size() - 1;
	for (int i = sz; i > id; i--) 
	{
		if (board->getPipesMatrix()[(int)list.at(i).x][(int)list.at(i).y])
		{
			board->getEmptyMatrix()[(int)list.at(i).x][(int)list.at(i).y]->setColor(Color3B::WHITE);
			board->getPipesMatrix()[(int)list.at(i).x][(int)list.at(i).y]->removeFromParentAndCleanup(true);
			board->getPipesMatrix()[(int)list.at(i).x][(int)list.at(i).y] = nullptr;
		}
		list.pop_back();
	}
}
int ScenePlay::calculateStarEarn()
{
	if (numOfMove <= numOfMaxMove)
	{
		return 3;
	}
	else
	{
		if ((numOfMove - numOfMaxMove) <= 2)
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}
}
void ScenePlay::checkPassLevelAndSaveData()
{
	if (board->calculatePercentFillBoard() == 100)
	{
		auto numOfStarToPlus = 0;
		auto starEarned = calculateStarEarn();
		auto dataSave = GameManager::getInstance()->getDataSave();
		auto levelSave = GameManager::getInstance()->getLevelState();
		if (levelSave.bestMove > numOfMove || levelSave.bestMove == 0)
		{
			levelSave.bestMove = numOfMove;
		}
		if (levelSave.starEarned < starEarned)
		{
			numOfStarToPlus = starEarned - levelSave.starEarned;
			levelSave.starEarned = starEarned;
			dataSave.starEachLevel[GameManager::getInstance()->getCurrentLevel()] = starEarned;
		}
		GameManager::getInstance()->setLevelState(levelSave);
		UserData::getInstance()->setNameLevelFileToSave(StringUtils::format("lv_%i.sav", GameManager::getInstance()->getCurrentLevel()));
		UserData::getInstance()->saveLevelIntoFile();
		if (GameManager::getInstance()->getCurrentLevel() > dataSave.passedLevel)
		{
			if (dataSave.starEachMode[GameManager::getInstance()->getCurrentMode()] < 0)
				dataSave.starEachMode[GameManager::getInstance()->getCurrentMode()] = numOfStarToPlus;
			else
				dataSave.starEachMode[GameManager::getInstance()->getCurrentMode()] += numOfStarToPlus;

			dataSave.currentLevel = GameManager::getInstance()->getCurrentLevel() + 1;
			dataSave.passedLevel = GameManager::getInstance()->getCurrentLevel();
		}
		else
		{
			dataSave.starEachMode[GameManager::getInstance()->getCurrentMode()] += numOfStarToPlus;
		}
		GameManager::getInstance()->setDataSave(dataSave);
		UserData::getInstance()->saveDataIntoFile();

		//reset all
		listVec2.clear();
		board->clearAllListColor();
		//set info for layer
		GameManager::getInstance()->setNumOfMove(numOfMove);
		GameManager::getInstance()->setStarEarn(starEarned);
		successLayer->show();
	}
}
void ScenePlay::updateMoveAndFillAfterTouchEnded()
{
	CCString *string_move = CCString::createWithFormat("Move: %i/%i", numOfMove,numOfMaxMove);
	moveLabel->setString(string_move->getCString());
	CCString *string_fillPercent = CCString::createWithFormat("Fill: %d%%", (int)board->calculatePercentFillBoard());
	fillPercentLabel->setString(string_fillPercent->getCString());
}
bool ScenePlay::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	bool begin = false;
	int x = 0;
	int y = 0;
	int sz = Sprite::create(BOARD)->getContentSize().width*scalePipe;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j] && board->getEmptyMatrix()[i][j]->getPosition().distance(touch->getLocation()) < sz/2)
			{
				if (board->getHolesMatrix()[i][j] || board->getPipesMatrix()[i][j])
				{
					if (board->getHolesMatrix()[i][j])
					{
						board->deleteAllPipeSameColor(board->getHolesMatrix()[i][j]->getColor());
						GameManager::getInstance()->setColorCurrent(board->getHolesMatrix()[i][j]->getColor());
					}
					else
					{
						GameManager::getInstance()->setColorCurrent(board->getPipesMatrix()[i][j]->getColor());
						auto temp = board->chooseListBaseColor(GameManager::getInstance()->getColorCurrent());
						listVec2 = temp;
					}
					x = i;
					y = j;
					begin = true;
					break;
				}
				else
				{
					return false;
				}
			}
		}
		if (begin)
		{
			break;
		}
	}
	if (begin)
	{
		handleTouch(x, y);
		board->setPreTouchCol(x);
		board->setPreTouchRow(y);
		return true;
	}
}

void ScenePlay::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
	bool movedToNewPos = false;
	int sz = Sprite::create(BOARD)->getContentSize().width*scalePipe;
	int preCol = board->getPreTouchCol();
	int preRow = board->getPreTouchRow();
	for (int i = 0; i < widthBoard; i++)
	{
		if (movedToNewPos)
		{
			break;
		}
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j] && board->getEmptyMatrix()[i][j]->getPosition().distance(touch->getLocation()) < sz/2)
			{
				if (board->isNeighbour(preCol, preRow, i, j))
				{
					if (!board->getMeetSameHole())
					{
						//Not has a hole on new touch
						if (!board->getHolesMatrix()[i][j])
						{
							if (board->getPipesMatrix()[i][j] && board->getPipesMatrix()[i][j]->getColor() != GameManager::getInstance()->getColorCurrent())
							{
								auto color = board->getPipesMatrix()[i][j]->getColor();
								Vec2 index = Vec2(i, j);
								int id = checkVec2InList(index, board->chooseListBaseColor(color));
								popVector(board->chooseListBaseColor(color), id);
							}
							board->setNextTouchCol(i);
							board->setNextTouchRow(j);
							movedToNewPos = true;
							break;
						}
						//Has a hole on new touch
						else
						{
							//meet end hole
							if (board->getHolesMatrix()[i][j]->getColor() == GameManager::getInstance()->getColorCurrent())
							{
								//Not has pipe on end hole
								if (!board->getPipesMatrix()[i][j])
								{
									board->setNextTouchCol(i);
									board->setNextTouchRow(j);
									board->setMeetSameHole(true);
									movedToNewPos = true;
									break;
								}
								//Has pipe on end hole 
								else
								{
									log("in 3");
									board->setNextTouchCol(i);
									board->setNextTouchRow(j);
									movedToNewPos = true;
									break;
								}
							}
						}
					}
					else
					{
						//Go backward after meet end hole
						if (!board->getHolesMatrix()[i][j] && board->getPipesMatrix()[i][j] && board->getPipesMatrix()[i][j]->getColor() == GameManager::getInstance()->getColorCurrent())
						{
							board->setNextTouchCol(i);
							board->setNextTouchRow(j);
							//board->getHolesMatrix()[board->getPreTouchCol()][board->getPreTouchRow()]->setChecked(false);
								//board->getPipesMatrix()[board->getPreTouchCol()][board->getPreTouchRow()]->setChecked(false);
							board->setMeetSameHole(false);
							movedToNewPos = true;
							break;
						}
					}
				}
			}
		}
		if (movedToNewPos)
		{
			break;
		}
	}
	if (movedToNewPos)
	{
		handleTouch(board->getNextTouchCol(), board->getNextTouchRow());
		/*if (board->getMeetSameHole())
		{
			board->getHolesMatrix()[board->getNextTouchCol()][board->getNextTouchRow()]->setChecked(true);
			board->getPipesMatrix()[board->getNextTouchCol()][board->getNextTouchRow()]->setChecked(true);
		}*/
		board->setPreTouchCol(board->getNextTouchCol());
		board->setPreTouchRow(board->getNextTouchRow());
		movedToNewPos = false;
		CCString *string_fillPercent = CCString::createWithFormat("Fill :%d%% ", (int)board->calculatePercentFillBoard());
		fillPercentLabel->setString(string_fillPercent->getCString());
	}
}
void ScenePlay::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
	COLOR curr = GameManager::getInstance()->getColorCurrent();
	int sz = Sprite::create(BOARD)->getContentSize().width*scalePipe;
	for (int i = 0; i < widthBoard; i++)
	{
		for (int j = 0; j < heightBoard; j++)
		{
			if (board->getEmptyMatrix()[i][j] && board->getEmptyMatrix()[i][j]->getPosition().distance(touch->getLocation()) < sz/2)
			{
				numOfMove++;
			}
		}
	}
	board->resetTouchPos();
	board->setMeetSameHole(false);
	board->markBoardByColor(listVec2);
	board->copyListBaseColor(curr, listVec2);
	listVec2.clear();
	updateMoveAndFillAfterTouchEnded();
	checkPassLevelAndSaveData();
}
void ScenePlay::onTouchCancelled(Touch *touch, cocos2d::Event *event)
{
	onTouchEnded(touch, event);
}



